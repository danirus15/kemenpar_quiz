<html>
<?php include "includes/head.php";?>
<?php
    if($_GET['theme']=="NaturalWonders"){
        $bodytheme="Natural-Wonders";
    }
    elseif($_GET['theme']=="CulturalWonders"){
        $bodytheme="Cultural-Wonders";
    }
    elseif($_GET['theme']=="AdventurousWonders"){
        $bodytheme="Adventurous-Wonders";
    }
    else{
        $bodytheme="Cultural-Wonders";
    }
?>
<body id="home" class="<?php echo $bodytheme;?>">
<div class="language-box">
    <div class="selectbox">Language - English</div>
    <div class="box-select">
        <div class="title"><b>Select your language</b></div>
        <a href="#">العربية</a>
        <a href="#">English</a>
        <a href="#">Français</a>
        <a href="#">日本語</a>
        <a href="#">한국어</a>
        <a href="#">简体中文</a>
        <a href="#">繁體中文</a>
    </div>
</div>
<div id="header2">

	<div class="img-cover fill-img">
		<img src="images/cover_komodo2.jpg" alt="">
	</div>
	<div class="overlay"></div>
    <div class="container">
        <a href="#" class="logo"><img src="images/logo.png" alt=""></a>
        <span class="title">Trips of Wonders</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="container quiz_con">
	<div class="title">Forget Password</div>
	<div class="desc">
	</div>
    <form action="quiz_login.php"  method="post" class="form">
        <div class="notif">
            Email address not available
        </div>
        <strong>Email</strong>
        <input type="text" name="email" class="input"  placeholder="Your Email">
        <div class="clearfix"></div>
        <input type="submit" value="Reset" class="btn_start">
    </form>
    <div class="cleafix pt20"></div>
    <b><a href="quiz_forget.php" class="l_blue">Register</a></b> | 
     <b><a href="quiz_connect_pre.php" class="l_blue">Login</a></b>
</div>
<?php include "includes/js.php";?>
</body>
</html>