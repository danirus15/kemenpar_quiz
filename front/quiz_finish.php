<html>
<?php include "includes/head.php";?>
<body id="home">
<!-- <div class="language-box">
    <div class="selectbox">Language - English</div>
    <div class="box-select">
        <div class="title"><b>Select your language</b></div>
        <a href="#">العربية</a>
        <a href="#">English</a>
        <a href="#">Français</a>
        <a href="#">日本語</a>
        <a href="#">한국어</a>
        <a href="#">简体中文</a>
        <a href="#">繁體中文</a>
    </div>
</div> -->
<div class="quiz_finish">

	<div class="img-cover fill-img">
		<img src="images/cover_komodo2.jpg" alt="">
	</div>
	<div class="overlay"></div>
    <div class="container">
        <div class="logo"><img src="images/logo.png" alt=""></div>
        <div class="title">Trips of Wonders</div>


        <div class="desc">
        	<b>Great! </b><br>
			You're finally done! 
        </div>
        <br>
        <div class="clearfix pt20"></div>
        YOUR SCORE<br>
        <div class="score">
        	850
        </div>
        <div class="clearfix pt20"></div>
        7 of 10 Correct Answer<br>
		Time: 05:33
		<br><br><br>
		Want to collect more points?<br>
        Just share this quiz with your friends by clicking below! <br>
        The more you share, the more you gain.<br>
		<div class="sosmed">
			<a href="#">
				<img src="images/ico_fb.png" alt="">
				<span>Share to Facebook</span>
			</a>
			<a href="#" class="tw">
				<img src="images/ico_tw.png" alt="">
				<span>Share to Twitter</span>
			</a>
		</div>
		<br><br>
		<a href="index2.php" class="link1">Back to Home</a>
    </div>
</div>

<?php include "includes/js.php";?>
</body>
</html>