<html>
<?php include "includes/head.php";?>
<body id="home">
<?php include "includes/header.php";?>
<div class="s_cover s_komodo">
	<h1>How to Play</h1>
	<div class="img_bg">
		<img src="images/theme1.png" alt="">
	</div>
</div>
<div id="how" class="section">
	<div class="container">
		<div class="howto">
			<div class="h1">
				<div class="text">
					<span class="no">1</span>
					<div class="title">Connect with Facebook</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<img src="images/01.png" alt="">
			</div>
			<div class="h2">
				<div class="text">
					<span class="no">2</span>
					<div class="title">Play the quiz and answer the questions </div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<img src="images/02.png" alt="">
			</div>
			<div class="h3">
				<div class="text">
					<span class="no">3</span>
					<div class="title">Finish the quiz, get a poin and share result</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<img src="images/03.png" alt="">
			</div>
			<div class="h4">
				<img src="images/04.png" alt="">
				<div class="clearfix"></div>
				<div class="text">
					<span class="no">4</span>
					<div class="title">
						<b>Win The Prize</b>
						Free Accomodation and vacation to Bali, Indonesia
					</div>
					<div class="clearfix"></div>
				</div>
				
				
			</div>
		</div>
		<a href="quiz_connect_pre.php" class="btn_start">Start Your Journey Now!</a>
	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>