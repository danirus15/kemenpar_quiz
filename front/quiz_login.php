<html>
<?php include "includes/head.php";?>
<?php
    if($_GET['theme']=="NaturalWonders"){
        $bodytheme="Natural-Wonders";
    }
    elseif($_GET['theme']=="CulturalWonders"){
        $bodytheme="Cultural-Wonders";
    }
    elseif($_GET['theme']=="AdventurousWonders"){
        $bodytheme="Adventurous-Wonders";
    }
    else{
        $bodytheme="Cultural-Wonders";
    }
?>
<body id="home" class="<?php echo $bodytheme;?>">
<div class="language-box">
    <div class="selectbox">Language - English</div>
    <div class="box-select">
        <div class="title"><b>Select your language</b></div>
        <a href="#">العربية</a>
        <a href="#">English</a>
        <a href="#">Français</a>
        <a href="#">日本語</a>
        <a href="#">한국어</a>
        <a href="#">简体中文</a>
        <a href="#">繁體中文</a>
    </div>
</div>
<div id="header2">

	<div class="img-cover fill-img">
		<img src="images/cover_komodo2.jpg" alt="">
	</div>
	<div class="overlay"></div>
    <div class="container">
        <a href="#" class="logo"><img src="images/logo.png" alt=""></a>
        <span class="title">Trips of Wonders</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="container quiz_con">
	<div class="title">Login</div>
	<div class="desc">
	</div>
    <form action="quiz_connect.php"  method="post" class="form">
        <div class="notif">
            Failed to login
        </div>
        <strong>Email</strong>
        <input type="text" name="email" class="input"  placeholder="Your Email">
        <strong>Password</strong>
        <input type="password" name="password" class="input"  placeholder="Password">
        <div class="clearfix"></div>
        <input type="submit" value="Login" class="btn_start">
    </form>
    <br>
    or login with facebook
    <div class="cleafix pt10"></div>
	<div class="sosmed">
		<a href="quiz_connect.php" class="fb2">
			<img src="images/ico_fb.png" alt="">
			<span>Connect with Facebook</span>
		</a>
	</div>
    <div class="cleafix pt10"></div>
    <b><a href="quiz_forget.php" class="l_blue">Forget Password</a></b><br><br>
     <b><a href="quiz_connect_pre.php" class="l_blue">Register</a></b>
</div>
<?php include "includes/js.php";?>
</body>
</html>