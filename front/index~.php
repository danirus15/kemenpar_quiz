<html>
<?php include "includes/head.php";?>
<body id="home">
<?php include "includes/header.php";?>
<div class="cover">
	<div class="img fill-img">
		<img src="images/cover_komodo2.jpg" alt="">
	</div>
	<div class="overlay"></div>
	<div class="text">
		<h1>Trips of Wonders</h1>
		<div class="desc">
			Straddling the equator, situated between the continents of Asia and Australia and between the Pacific and the Indian Oceans, Indonesia has many magnificent islands waiting to be explored.
		</div>
	</div>
</div>
<div id="how" class="section">
	<div class="container">
		<h2>Start Your Journey!</h2>
		<div class="desc">
			<p>Straddling the equator, situated between the continents of Asia and Australia and between the Pacific and the Indian Oceans, Indonesia has many magnificent islands waiting to be explored.
			</p>
			<p>Flores is the most fascinating and beautiful island. Long hidden in the shadows of its more famous neighbor Bali, the island of Flores is finally emerging as a unique destination of its own. So, after visiting the lair of the Komodo dragons, take time to marvel at some of the wonders of Flores. Here, you can swim in pristine lakes and waterfalls, dive at one of the 50 spectacular dive sites, go kayaking among craggy coasts and mangrove shores, explore mysterious caves and be warmly welcomed by the island’s people in their rituals, dances and daily life.
			</p>
			<p>
			Flores spells adventure, diving, eco-tours, and mountain climbing interspersed with visits to prehistoric heritage sites, traditional villages and cultural events. Find some of the world’s most exotic underwater life, dive in the pristine seas of Komodo, or swim along with huge manta rays, dolphins and dugongs in the island of Flores!
			</p>
		</div>
		<a href="quiz_connect_pre.php" class="btn_start">Start Your Journey Now!</a>
	</div>
</div>
<div class="toplead">
	<div class="container2">
		<h2>Top Participant</h2>
		<div class="top-pos">
			<ul>
				<li>
					<div class="no">1</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">2</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">3</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">4</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">5</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>
	</div>
</div>

<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>