<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> Wonderfull Indonesia </title>  
    
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/faficon.gif" type="image/x-icon">
     
    <link href="css/frame.style.css" rel="stylesheet" type="text/css" /> 
    <link href="css/fonts.css" rel="stylesheet" type="text/css" /> 
    <link href="css/style.css" rel="stylesheet" type="text/css" /> 
    <link href="css/style.responsive.css" rel="stylesheet" type="text/css" /> 
</head>
<?php
	if($_GET['theme']=="NaturalWonders"){
		$bodytheme="Natural-Wonders";
	}
	elseif($_GET['theme']=="CulturalWonders"){
		$bodytheme="Cultural-Wonders";
	}
	elseif($_GET['theme']=="AdventurousWonders"){
		$bodytheme="Adventurous-Wonders";
	}
	else{
		$bodytheme="Cultural-Wonders";
	}
?>
<body id="home" class="<?php echo $bodytheme;?>">
<div id="header">
    <div class="container">
        <a href="index.php" class="logo"><img src="images/logo.png" alt=""></a>
        <div class="menu-mobile menuclick">
        	<span></span>
        	<span></span>
        	<span></span>
        </div>
        <div class="menu-mobile menu-on">
        	<span></span>
        	<span></span>
        	<span></span>
        </div>
        <div class="overlay2"></div>
        <div class="nav">
            <a href="#home">Home</a>
            <a href="#how">How to Play</a>
            <a href="#prize">Prize</a>
            <a href="#terms">Terms & Conditions</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="login-area">
    	Hello Andi | <a href="#">Sign Out</a>
    </div>
    <div class="language-box">
        <div class="selectbox">Language - English</div>
        <div class="box-select">
            <div class="title"><b>Select your language</b></div>
            <a href="#">العربية</a>
            <a href="#">English</a>
            <a href="#">Français</a>
            <a href="#">日本語</a>
            <a href="#">한국어</a>
            <a href="#">简体中文</a>
            <a href="#">繁體中文</a>
        </div>
    </div>
</div>

<div class="cover" id="intro">
	<div class="img main-title">
		<img src="images/cover1.jpg" alt="">
	</div>
	<div class="overlay"></div>
	<div class="text">
		<h1>Trips of Wonders</h1>
		<div class="desc">
			Straddling the equator, situated between the continents of Asia and Australia and between the Pacific and the Indian Oceans, Indonesia has many magnificent islands waiting to be explored.
		</div>
	</div>
</div>
<div id="how" class="section">
	<div class="container animation-element bounce-up">
		<h2 class="a1">Start Your Journey!</h2>
		<div class="desc a1 a2">
			<p>The Wonderful Indonesia Trips of Wonders Quiz is organized by the Ministry of Tourism Indonesia. All you need to do is answer ten questions about Indonesia and you will have a chance to win a trip to Bali or Komodo Island.</p>
			<p>
			Participants from Southeast Asia and Asia Pacific can win a four day trip to Bali in Indonesia!
			</p>
			<p>
			Participants from the Americas, Africa, Middle East or Europe can win an awesome four day trip for two to Komodo Island! 
			</p>
			<p>
			So what are you waiting for? Click below and start your journey to Wonderful Indonesia.

						</p>
			<div class="howto">
				<div class="h1 a1 a3">
					<span class="no">1</span>
					<img src="images/01.png" alt="">
					<div class="clearfix"></div>
					<div class="title">Take this quiz. Win a trip!</div>
					<div class="clearfix"></div>
					
				</div>
				<div class="h1 a1 a4">
					<span class="no">2</span>
					<img src="images/02.png" alt="">
					<div class="clearfix"></div>
					<div class="title">Just Answer These 10 Questions and Get a Chance to Win a Trip to Bali/ Labuan Bajo. Good Luck!</div>
					<div class="clearfix"></div>
				</div>
				<div class="h1 a1 a5">
					<span class="no">3</span>
					<img src="images/03.png" alt="">
					<div class="clearfix"></div>
					<div class="title">The more you share, the more you get. </div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<span class="a1 a6">So what are you waiting for? Click below and start your journey to Wonderful Indonesia.</span>
			<br>
			<a href="quiz_connect_pre.php" class="btn_start a1 a6">Start Your Journey Now!</a>
		</div>
	</div>
	<div class="clearfix batas"></div>
</div>
<div class="section s_prize" id="prize">
    <div class="container prize animation-element bounce-up">
		<h2 class="a1">Prize</h2>
		<div class="prize">
			<img src="images/04.png" alt="" class="a1 a2">
			<div class="clearfix"></div>
			<div class="text a1 a3">
				<div class="title">
					<b>Win The Prize</b>
					Free Accomodation and vacation to Bali, Indonesia
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<br>
	<div id="img_gal" class="img_gal animation-element bounce-up">

		<?php
		  if($_GET["theme"]=="NaturalWonders"){
		  	$img = array("1" => "01.jpg", 
		  		"2" => "02.jpg",
		  		"3" => "03.jpg",
		  		"4" => "04.jpg",
		  		"5" => "05.jpg",
		  		"6" => "06.jpg",
		  		"7" => "07.jpg",
		  		"8" => "08.jpg");
		  }
		  elseif($_GET["theme"]=="CulturalWonders"){
		  	$img = array("1" => "04.jpg", 
		  		"2" => "06.jpg",
		  		"3" => "07.jpg",
		  		"4" => "08.jpg",
		  		"5" => "09.jpg",
		  		"6" => "01.jpg",
		  		"7" => "02.jpg",
		  		"8" => "03.jpg");
		  }
		  else{
		  	$img = array("1" => "09.jpg", 
		  		"2" => "01.jpg",
		  		"3" => "08.jpg",
		  		"4" => "04.jpg",
		  		"5" => "05.jpg",
		  		"6" => "06.jpg",
		  		"7" => "07.jpg",
		  		"8" => "08.jpg");
		  }

          foreach ($img as $i => $img2) {
            echo'<div class="ratio4_3 box_img a1 a'.$i.'">
				<div class="img_con fill-img"> <img src="gal/'.$img2.'"/> </div>
			</div>';
        }
        ?>
	</div>
	<div class="clearfix"></div>
</div>
<div class="section animation-element bounce-up" id="participant">
	<div class="container2">
		<h2 class="a1">Top Participant</h2>
		<div class="top-pos a1 a2">
			<ul>
				<li>
					<div class="no">1</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">2</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">3</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">4</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="no">5</div>
					<div class="name">Antonio Conte Joko</div>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="section s_term animation-element bounce-up" id="terms">
	<div class="content">
		<div class="container">
			<h2 class="a1">Terms & Conditions</h2>
			<div class="detail_text a1 a2">
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
				</p>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
				</p>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
				</p>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
				</p>
			</div>
		</div>
	</div>
</div>


<div id="footer">
	<div class="container">
			Copyright &copy; 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
		<div class="clearfix"></div>
	</div>
</div>
<script type="text/javascript" src="js/jquery.js"></script> 
<script type="text/javascript" src="js/sticky-scroll.js"></script>  
<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>  
<script type="text/javascript" src="js/jquery-imagefill.js"></script>  
<script type="text/javascript" src="js/jquery.fallings.js"></script>
<script type="text/javascript" src="js/controller.wp.js"></script>
</body>
</html>