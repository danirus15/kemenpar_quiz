<div id="header">
    <div class="container">
        <a href="#" class="logo"><img src="images/logo.png" alt=""></a>
        
        <div class="nav">
            <a href="index.php">Home</a>
            <a href="how.php">How to Play</a>
            <a href="terms.php">Terms & Conditions</a>
            <a href="prize.php">Prize</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="language-box">
        <div class="selectbox">Language - English</div>
        <div class="box-select">
            <div class="title"><b>Select your language</b></div>
            <a href="#">العربية</a>
            <a href="#">English</a>
            <a href="#">Français</a>
            <a href="#">日本語</a>
            <a href="#">한국어</a>
            <a href="#">简体中文</a>
            <a href="#">繁體中文</a>
        </div>
    </div>
</div>
