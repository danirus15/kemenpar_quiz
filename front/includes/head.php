<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title> Wonderfull Indonesia </title>  
    
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/faficon.gif" type="image/x-icon">
     
    <link href="css/frame.style.css" rel="stylesheet" type="text/css" /> 
    <link href="css/fonts.css" rel="stylesheet" type="text/css" /> 
    <link href="css/style.css" rel="stylesheet" type="text/css" /> 
    <link href="css/style.responsive.css" rel="stylesheet" type="text/css" /> 
    
    <script type="text/javascript" src="js/jquery.min.js"></script>  
</head>