<html>
<?php include "includes/head.php";?>
<?php
    if($_GET['theme']=="NaturalWonders"){
        $bodytheme="Natural-Wonders";
    }
    elseif($_GET['theme']=="CulturalWonders"){
        $bodytheme="Cultural-Wonders";
    }
    elseif($_GET['theme']=="AdventurousWonders"){
        $bodytheme="Adventurous-Wonders";
    }
    else{
        $bodytheme="Cultural-Wonders";
    }
?>
<body id="home" class="<?php echo $bodytheme;?>">
<div id="header2">
	<div class="img-cover fill-img">
		<img src="images/cover_komodo2.jpg" alt="">
	</div>
	<div class="overlay"></div>
    <div class="container">
        <a href="#" class="logo"><img src="images/logo.png" alt=""></a>
        <span class="title">Trips of Wonders</span>

        <div class="clearfix"></div>
    </div>
</div>
<div class="container quiz_con quiz_con2">
	<form action="quiz_finish.php">
		<div class="q">
			Komodo Dragon habitat is located in Flores Island ?
		</div>
		<div class="a radiocheck">
			<label>
				<input type="radio" name="gender">
				<div class="check"></div>
				<div class="a1">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero nostrum vel iste Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero nostrum vel iste
				</div>
				<div class="clearfix"></div>
			</label>
			<label>
				<input type="radio" name="gender">
				<div class="check"></div>
				<div class="a1">
					Lorem ipsum dolor sit amet,  elit. Libero nostrum vel iste
				</div>
				<div class="clearfix"></div>
			</label>
			<label>
				<input type="radio" name="gender">
				<div class="check"></div>
				<div class="a1">
					Consectetur adipisicing elit. Libero nostrum vel iste
				</div>
				<div class="clearfix"></div>
			</label>
		</div>
		<div class="hints">
			Check qlue on <a href="http://indonesia.travel/en/post/banyuwangi-getting-off-the-beaten-path" target="_blank">http://indonesia.travel/en/post/banyuwangi-getting-off-the-beaten-path</a>
		</div>
		<div class="clearfix pt20"></div>
		<div align="center">
			<input type="submit" value="Next Question" class="btn_start">
		</div>
		<div class="time_count">
			<span>TIME</span>
			<input type='text' name='timer' class='form-control timer' placeholder='00.00' />
		</div>
	</form>
</div>
<?php include "includes/js.php";?>
</body>
</html>