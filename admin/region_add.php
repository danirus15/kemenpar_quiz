<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php 
  $page="region";
?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Add/Edit Region</h3>
                      <div class="clearfix"></div>
                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                      
                    </header>
                    <div class="panel-body">
                      <form class="form-horizontal" method="get" action="region.php">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Region Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control">
                          </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Language Default</label>
                          <div class="col-sm-10">
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                English
                              </label>
                            </div>
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                Fran&ccedil;ais
                              </label>
                            </div>
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                日本語
                              </label>
                            </div>
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                한국어
                              </label>
                            </div>
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                简体中文
                              </label>
                            </div>
                            <div class="checkbox i-checks">
                              <label>
                                <input type="radio" value="" name="language">
                                <i></i>
                                繁體中文
                              </label>
                            </div>

                          </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                            <button type="submit" class="btn btn-default">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>