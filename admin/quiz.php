<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php 
  $page="quiz";
?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Quiz Period</h3>
                      <a href="quiz_form.php" class="btn btn-s-md btn-primary btn-rounded fr">Tambah</a>
                      <div class="clearfix"></div>

                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                   <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="20%">Time Period</th>
                        <th width="20%">Region</th>
                        <th width="30%">Template</th>
                        <th width="10%">Status</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>12 July 2016 - 12 August 2016</td>
                        <td>South East Asia</td>
                        <td>Natural Wonders</td>
                        <td><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Active" data-original-title="Active" class="active"><img src="images/checked-2.png"></a></td>
                        <td class="action">
                          <a href="quiz_form.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>12 July 2016 - 12 August 2016</td>
                        <td>Asia Pacific</td>
                        <td>Cultural Wonders</td>
                        <td><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactive" data-original-title="Deactive" class="active"><img src="images/checked-1.png"></a></td>
                        <td class="action">
                          <a href="quiz_form.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit" ><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>12 July 2016 - 12 August 2016</td>
                        <td>South East Asia</td>
                        <td>Adventurous Wonders</td>
                        <td><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactive" data-original-title="Deactive" class="active"><img src="images/checked-1.png"></a></td>
                        <td class="action">
                          <a href="quiz_form.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                     
                      
                    </tbody>
                  </table>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>