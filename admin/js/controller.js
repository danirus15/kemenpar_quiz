$(function() {
    $("#uploadFile").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#imagePreview").css("background-image", "url("+this.result+")");
            }
        }
    });
});

function reset () {
    alertify.set({
        labels : {
            ok     : "OK",
            cancel : "Cancel"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}
function deletes () {
    alertify.set({
        labels : {
            ok     : "Yes",
            cancel : "No"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}

$(".alert").on( 'click', function () {
    reset();
    alertify.alert("This is an alert dialog");
    return false;
});
$(".delete").on( 'click', function () {
    var target  = $(this).attr("href");
    deletes();
    alertify.confirm("Are you sure to delete this", function (e) {
        if (e) {
            window.location = target;
            //alert("ok");
        } else {
            // user clicked "cancel"
            //alert("cancel");
        }
    });
    //alertify.confirm("Are you sure to delete this", function (e) {});
    return false;
    
});



function popup(pageURL, title) {
  var w = 1000;
  var h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$( document ).ready(function() {
    $(".add-report-kar").click(function() {
        $("#kar-2").removeClass();
        $(".add-report-kar").hide();
    });
    $(".add-report-per").click(function() {
        $("#per-2").removeClass();
        $(".add-report-per").hide();
    });


});

tinymce.init({
    selector: "textarea.tinymc"
});



