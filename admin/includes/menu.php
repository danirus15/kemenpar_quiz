<?php

  $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

  if($page=="home") {$home="active";}
  elseif($page=="quiz") {$quiz="active";}
  elseif($page=="name") {$name="active";}
  elseif($page=="statik") {
    $statik="active";

    if($page2=="how") {  $how="active"; }
    elseif($page2=="terms") {  $terms="active"; }
    else{}

  }
  elseif($page=="region") {$region="active";}
  elseif($page=="participant") {
    $participant="active";

    if($page2=="part1") {  $part1="active"; }
    elseif($page2=="part2") {  $part2="active"; }
    else{}

  }
  elseif($page=="question") {
    $question="active";

    if($page2=="easy") {  $easy="active"; }
    elseif($page2=="medium") {  $medium="active"; }
    else{}

  }

  else{}
?>


<aside class="bg-light aside-md hidden-print" id="nav">          
  <section class="vbox">
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">               


        <!-- nav -->                 
        <nav class="nav-primary hidden-xs">
          <ul class="nav nav-main" data-ride="collapse">
            <!-- <li  class="<?php echo $home;?>">
              <a href="index.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Dashboard</span>
              </a>
            </li> -->
            <li  class="<?php echo $quiz;?>">
              <a href="quiz.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Quiz Period</span>
              </a>
            </li>
            <li class="<?php echo $statik;?>">
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>                                 
                <i class="i i-grid2 icon"></i>

                <span class="font-bold">Statik Page</span>
              </a>
              <ul class="nav dk dk2" >
                <li class="<?php echo $how;?>">
                  <a href="how.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>How to Play Page</span>
                  </a>
                </li>
                </li>
                <li class="<?php echo $terms;?>">
                  <a href="term.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Terms & Condition Page</span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="<?php echo $question;?>">
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>                                 
                <i class="i i-grid2 icon"></i>

                <span class="font-bold">Question</span>
              </a>
              <ul class="nav dk dk2" >
                <li class="<?php echo $easy;?>">
                  <a href="q_easy.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Easy</span>
                  </a>
                </li>
                <li class="<?php echo $medium;?>">
                  <a href="q_medium.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Medium</span>
                  </a>
                </li>
              </ul>
            </li>
            <li  class="<?php echo $region;?>">
              <a href="region.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Region</span>
              </a>
            </li>
            <li  class="<?php echo $name;?>">
              <a href="name.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Indonesian Name</span>
              </a>
            </li>
             <li class="<?php echo $participant;?>">
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>                                 
                <i class="i i-grid2 icon"></i>

                <span class="font-bold">Participant</span>
              </a>
              <ul class="nav dk dk2" >
                <li class="<?php echo $part1;?>">
                  <a href="part1.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Period 1</span>
                  </a>
                </li>
                <li class="<?php echo $part2;?>">
                  <a href="part1.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Period 2</span>
                  </a>
                </li>
                <li class="<?php echo $part3;?>">
                  <a href="part1.php" class="auto">                 
                    <i class="i i-dot"></i>
                    <span>Period 3</span>
                  </a>
                </li>
              </ul>
            </li>
            <li  class="<?php echo $name;?>">
              <a href="member.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Admin Member</span>
              </a>
            </li>
            
            
            
          </ul>
          <div class="line dk hidden-nav-xs"></div>
        </nav>
        <!-- / nav -->
      </div>
    </section>
    
    <footer class="footer hidden-xs no-padder text-center-nav-xs">
      <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
        <i class="i i-logout"></i>
      </a>
      <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
        <i class="i i-circleleft text"></i>
        <i class="i i-circleright text-active"></i>
      </a>
    </footer>
  </section>
</aside>