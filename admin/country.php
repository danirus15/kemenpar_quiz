<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php 
  $page="region";
?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <a href="region.php" class="fl mr10"><img src="images/back.png" alt="" height="25"></a>
                      <h3 class="m-b-xs text-black fl">Country / <?php echo $_GET["prov"];?></h3>
                      <a href="country_add.php" class="btn btn-s-md btn-primary btn-rounded fr">Tambah</a>
                      <div class="clearfix"></div>

                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                   <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="30%">Country</th>
                        <th width="15%">Default Language</th>
                        <th width="15%">IP</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Singapure</td>
                        <td>English</td>
                        <td>192.231.019.22</td>
                        <td class="action">
                          <a href="country_add.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>Malaysia</td>
                        <td>English</td>
                        <td>192.231.019.42</td>
                        <td class="action">
                          <a href="country_add.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>China</td>
                        <td>日本語</td>
                        <td>192.231.019.222</td>
                        <td class="action">
                          <a href="country_add.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                      
                    </tbody>
                  </table>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>