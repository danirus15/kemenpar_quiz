<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php $page="member";?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Member</h3>
                      <a href="member_add.php" class="btn btn-s-md btn-primary btn-rounded fr">Tambah</a>
                      <div class="clearfix"></div>

                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                   <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="40%">Nama</th>
                        <th width="20%">Email</th>
                        <th width="15%">Group</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Rudianto</td>
                        <td>rudianto@gamil.com</td>
                        <td>Super Admin</td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>Santo</td>
                        <td>rudianto@gamil.com</td>
                        <td>Admin</td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>Rudianto</td>
                        <td>rudianto@gamil.com</td>
                        <td>Member</td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>