<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html lang="en" class="app">
<?php 
  $page="question";
  $page2="easy";
?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Add/Edit Question</h3>
                      <div class="clearfix"></div>
                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                    <header class="panel-heading bg-light">
                      <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#english" data-toggle="tab">English</a></li>
                        <li><a href="#arab" data-toggle="tab">العربية</a></li>
                        <li><a href="#france" data-toggle="tab">Fran&ccedil;ais</a></li>
                        <li><a href="#japan" data-toggle="tab">日本語</a></li>
                        <li><a href="#korea" data-toggle="tab">한국어</a></li>
                        <li><a href="#china" data-toggle="tab">简体中文</a></li>
                        <li><a href="#taiwan" data-toggle="tab">繁體中文</a></li>
                      </ul>
                    </header>
                    <div class="panel-body">
                      <form class="form-horizontal" method="get" action="region.php">
                      <div class="tab-content">
                          <!-- s:english -->
                          <div class="tab-pane active" id="english">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:english -->
                          <!-- s:arab -->
                          <div class="tab-pane" id="arab">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:arab -->
                          <!-- s:france -->
                          <div class="tab-pane" id="france">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>

                          </div>
                          <!-- e:france -->
                          <!-- s:japan -->
                          <div class="tab-pane" id="japan">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:japan -->
                          <!-- s:korea -->
                          <div class="tab-pane" id="korea">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:korea -->
                           <!-- s:china -->
                          <div class="tab-pane" id="china">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:china -->
                          <!-- s:taiwan -->
                          <div class="tab-pane" id="taiwan">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Answer</label>
                              <div class="col-sm-10 answer">
                                <div class="a1">A</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">B</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                                <div class="a1">C</div>
                                <div class="a2"><input type="text" class="form-control"></div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Hints</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control">
                              </div>
                            </div>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                          </div>
                          <!-- e:taiwan -->
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Corect Answer</label>
                          <div class="col-sm-10">
                            <label class="checkbox-inline">
                              <input type="radio" id="inlineCheckbox1" name="corect"> A
                            </label>
                            <label class="checkbox-inline">
                              <input type="radio" id="inlineCheckbox1"  name="corect"> B
                            </label>
                            <label class="checkbox-inline">
                              <input type="radio" id="inlineCheckbox1" name="corect"> C
                            </label>
                          </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                            <button type="submit" class="btn btn-default">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>