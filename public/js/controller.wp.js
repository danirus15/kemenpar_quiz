var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if (window_top_position > (element_top_position-450)) {
        $element.addClass('in-view');
    } else {
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');



  jQuery( document ).ready(function() {

      $(".main-title").fallings({
          velocity: 1.2
      });
      $(".parax2").fallings({
          velocity: 1.2
      });

      $(".primary-image").fallings({
          velocity: .5,
          bgParallax: true,
          bgPercent: '50%'
      });

      $(".footer-image").fallings({
          velocity: .3,
          initialPosition: 100,
          bgParallax: true,
          bgPercent: '50%'
      });

  });


  $(document).ready(function(){
    $('.fill-img').imagefill(); 
  });

  if ($(window).width() > 500) {
    function init() {
      window.addEventListener('scroll', function(e){
          var distanceY = window.pageYOffset || document.documentElement.scrollTop,
              shrinkOn = 150,
              header = document.querySelector("#header");
          if (distanceY > shrinkOn) {
              classie.add(header,"smaller");
              $("body").css("padding-top","100px");
          } else {
              if (classie.has(header,"smaller")) {
                  classie.remove(header,"smaller");
                  $("body").css("padding-top","0");
              }
          }
      });
    }
    window.onload = init();
    //lert();
  }
  else{
     $('.menuclick').click(function(){
        $("#header").addClass("nav-on");
        $(".menu-on").show();
        $(".menuclick").hide();
        $("body").css("overflow","hidden");
     });
     $('.menu-on').click(function(){
        $("#header").removeClass("nav-on");
        $(".menuclick").show();
        $(".menu-on").hide();
        $("body").css("overflow","scroll");
     });
     $('.overlay2').click(function(){
        $("#header").removeClass("nav-on");
        $(".menuclick").show();
        $(".menu-on").hide();
        $("body").css("overflow","scroll");
     });
  }


  $('.radiocheck label').click(function(){
    $(".radiocheck label").removeClass("selected");
    $(this).addClass("selected");
  });
  $('.radiocheck label.kosong').click(function(){
    $(".radiocheck label").removeClass("selected");
    $(this).addClass("selected");
  });

  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        target2 = $(this.hash);
        var hashtag = $(this).attr("href");
        
        if (target.length) {
            var tinggi = target.offset().top - 100;
            $('html,body').animate({
              scrollTop: tinggi
            }, 700);
       
          

            return false;

        }
      }
    });
  });

  (function(){
      var hasTimer = false;
      $('.start-timer-btn').ready(function() {
        hasTimer = true;
        $('.timer').timer({
          editable: true,
          format: '%M:%S'
        });
        $(this).addClass('hidden');
        $('.pause-timer-btn, .remove-timer-btn').removeClass('hidden');
      });

    })();




