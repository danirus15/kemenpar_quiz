<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title>Login</title>
	<link href="<?=css_uri();?>login.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="container">
		<form action="<?=base_url();?>cekLogin" method="post" class="form_login form">
			<div align="center"><img src="<?=images_uri();?>logo.png" alt="" height="120"></div>
		<?php if($this->session->flashdata('erroLogin')): ?>
			<div class="notif"><?=$this->session->flashdata('erroLogin');?></div>
		<?php endif; ?>
			<strong>username</strong>
			<input type="text" class="input" name="username" value="" required>
			<br>
			<strong>password</strong>
			<input type="password" class="input" name="passwd" value="" required>
			<br>
			<input type="submit" value="Login" class="btn">
		</form>
	</div>

	<script src='<?=js_uri();?>js.js'></script>
	<script src='<?=js_uri();?>imgLiquid-min.js'></script>
	<script src='<?=js_uri();?>controller.js'></script>
</body>
</html>
