<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">How to Play Page</h3>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<div class="panel-body">
											<form method="post" class="form-horizontal" action="<?=base_url();?>howto/updatepost">
												<section class="panel panel-default">
													<header class="panel-heading bg-light">
														<ul class="nav nav-tabs nav-justified">
															<li class="active"><a href="#english" data-toggle="tab">English</a></li>
															<li><a href="#arab" data-toggle="tab">العربية</a></li>
															<li><a href="#france" data-toggle="tab">Fran&ccedil;ais</a></li>
															<li><a href="#japan" data-toggle="tab">日本語</a></li>
															<li><a href="#korea" data-toggle="tab">한국어</a></li>
															<li><a href="#china" data-toggle="tab">简体中文</a></li>
															<li><a href="#taiwan" data-toggle="tab">繁體中文</a></li>
														</ul>
													</header>
													<div class="panel-body">
														<div class="tab-content">
															<!-- s:english -->
															<div class="tab-pane active" id="english">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_en" value="<?=$itemData['title_en'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_en" id="" cols="30" rows="10"><?=$itemData['description_en'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_en" value="<?=$itemData['step1_en'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_en" value="<?=$itemData['step2_en'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_en" value="<?=$itemData['step3_en'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_en" value="<?=$itemData['title_prize_en'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_en" value="<?=$itemData['desc_prize_en'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:english -->
															<!-- s:arab -->
															<div class="tab-pane" id="arab">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_ar" value="<?=$itemData['title_ar'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_ar" id="" cols="30" rows="10"><?=$itemData['description_ar'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_ar" value="<?=$itemData['step1_ar'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_ar" value="<?=$itemData['step2_ar'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_ar" value="<?=$itemData['step3_ar'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_ar" value="<?=$itemData['title_prize_ar'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_ar" value="<?=$itemData['desc_prize_ar'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:arab -->
															<!-- s:france -->
															<div class="tab-pane" id="france">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_fr" value="<?=$itemData['title_fr'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_fr" id="" cols="30" rows="10"><?=$itemData['description_fr'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_fr" value="<?=$itemData['step1_fr'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_fr" value="<?=$itemData['step2_fr'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_fr" value="<?=$itemData['step3_fr'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_fr" value="<?=$itemData['title_prize_fr'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_fr" value="<?=$itemData['desc_prize_fr'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:france -->
															<!-- s:japan -->
															<div class="tab-pane" id="japan">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_ja" value="<?=$itemData['title_ja'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_ja" id="" cols="30" rows="10"><?=$itemData['description_ja'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_ja" value="<?=$itemData['step1_ja'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_ja" value="<?=$itemData['step2_ja'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_ja" value="<?=$itemData['step3_ja'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_ja" value="<?=$itemData['title_prize_ja'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_ja" value="<?=$itemData['desc_prize_ja'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:japan -->
															<!-- s:korea -->
															<div class="tab-pane" id="korea">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_ko" value="<?=$itemData['title_ko'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_ko" id="" cols="30" rows="10"><?=$itemData['description_ko'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_ko" value="<?=$itemData['step1_ko'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_ko" value="<?=$itemData['step2_ko'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_ko" value="<?=$itemData['step3_ko'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_ko" value="<?=$itemData['title_prize_ko'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_ko" value="<?=$itemData['desc_prize_ko'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:korea -->
															<!-- s:china -->
															<div class="tab-pane" id="china">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_zh-cn" value="<?=$itemData['title_zh-cn'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_zh-cn" id="" cols="30" rows="10"><?=$itemData['description_zh-cn'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_zh-cn" value="<?=$itemData['step1_zh-cn'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_zh-cn" value="<?=$itemData['step2_zh-cn'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_zh-cn" value="<?=$itemData['step3_zh-cn'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_zh-cn" value="<?=$itemData['title_prize_zh-cn'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_zh-cn" value="<?=$itemData['desc_prize_zh-cn'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:china -->
															<!-- s:taiwan -->
															<div class="tab-pane" id="taiwan">
																<div class="form-group">
																	<label class="col-sm-2 control-label">Title</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control" name="title_zh-tw" value="<?=$itemData['title_zh-tw'];?>">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group ">
																	<label class="col-sm-2 control-label">Description</label>
																	<div class="col-md-8">
																		<textarea class="input-sm form-control" name="description_zh-tw" id="" cols="30" rows="10"><?=$itemData['description_zh-tw'];?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">1. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step1_zh-tw" value="<?=$itemData['step1_zh-tw'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">2. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step2_zh-tw" value="<?=$itemData['step2_zh-tw'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">3. </label>
																	<div class="col-sm-10">
																		<input type="text" name="step3_zh-tw" value="<?=$itemData['step3_zh-tw'];?>" class="form-control">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
																<div class="form-group">
																	<label class="col-sm-2 control-label">4. </label>
																	<div class="col-sm-10">
																		<input type="text" name="title_prize_zh-tw" value="<?=$itemData['title_prize_zh-tw'];?>" class="form-control" placeholder="title"><br>
																		<input type="text" name="desc_prize_zh-tw" value="<?=$itemData['desc_prize_zh-tw'];?>" class="form-control" placeholder="prize">
																	</div>
																</div>
																<div class="line line-dashed b-b line-lg pull-in"></div>
															</div>
															<!-- e:taiwan -->
														</div>
													</div>
												</section>
												<div class="form-group">
													<div class="col-sm-4 col-sm-offset-2">
														<input type="hidden" name="id" value="<?=$ids;?>">
														<button type="submit" class="btn btn-primary">Save changes</button>
													</div>
												</div>
											</form>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
