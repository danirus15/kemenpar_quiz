<aside class="bg-light aside-md hidden-print" id="nav">
	<section class="vbox">
		<section class="w-f scrollable">
			<div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
				<!-- nav -->
				<nav class="nav-primary hidden-xs">
					<ul class="nav nav-main" data-ride="collapse">
						<?php /*<li class="<?php if($page=="dashboard") echo 'active';?>">
							<a href="<?=base_url();?>dashboard" class="auto">
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Dashboard</span>
							</a>
						</li>*/ ?>
						<li class="<?php if($page=="quiz") echo 'active';?>">
							<a href="<?=base_url();?>quizperiod" class="auto">
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Quiz Period</span>
							</a>
						</li>
						<li class="<?php if($pages=="statik") echo 'active';?>">
							<a href="#" class="auto">
								<span class="pull-right text-muted">
									<i class="i i-circle-sm-o text"></i>
									<i class="i i-circle-sm text-active"></i>
								</span>
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Statik Page</span>
							</a>
							<ul class="nav dk dk2">
								<li class="<?php if($page=="howto") echo 'active';?>">
									<a href="<?=base_url();?>howto" class="auto">                 
										<i class="i i-dot"></i>
										<span>How to Play Paket 1</span>
									</a>
								</li>
								<li class="<?php if($page=="howto2") echo 'active';?>">
									<a href="<?=base_url();?>howto/2" class="auto">                 
										<i class="i i-dot"></i>
										<span>How to Play Paket 2</span>
									</a>
								</li>
								<li class="<?php if($page=="howto3") echo 'active';?>">
									<a href="<?=base_url();?>howto/3" class="auto">                 
										<i class="i i-dot"></i>
										<span>How to Play  Paket 3</span>
									</a>
								</li>
								<li class="<?php if($page=="term") echo 'active';?>">
									<a href="<?=base_url();?>term" class="auto">                
										<i class="i i-dot"></i>
										<span>Terms & Condition Paket 1</span>
									</a>
								</li>
								<li class="<?php if($page=="term2") echo 'active';?>">
									<a href="<?=base_url();?>term/2" class="auto">                
										<i class="i i-dot"></i>
										<span>Terms & Condition Paket 2</span>
									</a>
								</li>
								<li class="<?php if($page=="term3") echo 'active';?>">
									<a href="<?=base_url();?>term/3" class="auto">                
										<i class="i i-dot"></i>
										<span>Terms & Condition Paket 3</span>
									</a>
								</li>
							</ul>
						</li>
						<li class="<?php if($pages=="question") echo 'active';?>">
							<a href="#" class="auto">
								<span class="pull-right text-muted">
									<i class="i i-circle-sm-o text"></i>
									<i class="i i-circle-sm text-active"></i>
								</span>
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Question</span>
							</a>
							<ul class="nav dk dk2">
								<li class="<?php if($page=="easy") echo 'active';?>">
									<a href="<?=base_url();?>qeasy" class="auto">
										<i class="i i-dot"></i>
										<span>Easy</span>
									</a>
								</li>
								<li class="<?php if($page=="medium") echo 'active';?>">
									<a href="<?=base_url();?>qmedium" class="auto">
										<i class="i i-dot"></i>
										<span>Medium</span>
									</a>
								</li>
							</ul>
						</li>
						<li class="<?php if($page=="region") echo 'active';?>">
							<a href="<?=base_url();?>region" class="auto">
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Region</span>
							</a>
						</li>
						<li class="<?php if($page=="name") echo 'active';?>">
							<a href="<?=base_url();?>name" class="auto">
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Indonesian Name</span>
							</a>
						</li>
						<li class="<?php if($pages=="participant") echo 'active';?>">
							<a href="#" class="auto">
								<span class="pull-right text-muted">
									<i class="i i-circle-sm-o text"></i>
									<i class="i i-circle-sm text-active"></i>
								</span>
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Participant</span>
							</a>
							<ul class="nav dk dk2">
						<?php
						if(count($dataPeriod)>0):
							foreach($dataPeriod as $period):
						?>
								<li class="<?php if($page==$period['id']) echo 'active';?>">
									<a href="<?=base_url();?>participant/<?=$period['id'];?>" class="auto">                 
										<i class="i i-dot"></i>
										<span><?=$period['region_quiz'];?></span>
									</a>
								</li>
						<?php
							endforeach;
						endif;
						?>
							</ul>
						</li>
						<li  class="<?php if($page=="member") echo 'active';?>">
							<a href="<?=base_url();?>member" class="auto">
								<i class="i i-grid2 icon"></i>
								<span class="font-bold">Admin Member</span>
							</a>
						</li>
					</ul>
					<div class="line dk hidden-nav-xs"></div>
				</nav>
				<!-- / nav -->
			</div>
		</section>

		<footer class="footer hidden-xs no-padder text-center-nav-xs">
			<a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
				<i class="i i-logout"></i>
			</a>
			<a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
				<i class="i i-circleleft text"></i>
				<i class="i i-circleright text-active"></i>
			</a>
		</footer>
	</section>
</aside>
