<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Member</h3>
											<a href="<?=base_url();?>member/add" class="btn btn-s-md btn-primary btn-rounded fr">Add</a>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<div class="table-responsive">
											<table class="table table-striped m-b-none" data-ride="datatables">
												<thead>
													<tr>
														<th width="40%">Nama</th>
														<th width="10%">Action</th>
													</tr>
												</thead>
												<tbody>
											<?php
											if($totalData>0):
												foreach($itemData as $row):
											?>
													<tr>
														<td><?=$row['username'];?></td>
														<td class="action">
															<a href="<?=base_url();?>member/edit/<?=$row['idAdmin'];?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit">
																<img src="<?=images_uri();?>ico_edit.png" alt="">
															</a>
															<a onclick="return confirm('Are you sure want to remove <?=$row['username'];?>?');" href="<?=base_url();?>member/delete/<?=$row['idAdmin'];?>" data-toggle="tooltip" data-placement="top" title="">
																<img src="<?=images_uri();?>ico_del.png" alt="">
															</a>
														</td>
													</tr>
											<?php
												endforeach;
											endif;
											?>
												</tbody>
											</table>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
