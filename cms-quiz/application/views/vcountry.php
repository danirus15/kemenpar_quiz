<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<a href="<?=base_url();?>region" class="fl mr10"><img src="<?=images_uri();?>back.png" alt="" height="25"></a>
											<h3 class="m-b-xs text-black fl">Country / <?=$dataRegion['region_name'];?></h3>
											<a href="<?=base_url();?>country/add/<?=$this->uri->segment(2);?>" class="btn btn-s-md btn-primary btn-rounded fr">Add</a>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<div class="table-responsive">
											<table class="table table-striped m-b-none" data-ride="datatables">
												<thead>
													<tr>
														<th width="30%">Country</th>
														<th width="15%">Default Language</th>
														<th width="15%">IP</th>
														<th width="10%">Action</th>
													</tr>
												</thead>
												<tbody>
											<?php
											if($totalData>0):
												foreach($itemData as $row):
											?>
													<tr>
														<td><?=$row['country_name'];?></td>
														<td><?=language($row['def_lang']);?></td>
														<td><?=$row['ip'];?></td>
														<td class="action">
															<a href="<?=base_url();?>country/edit/<?=$this->uri->slash_segment(2,'trailing').$row['country_id'];?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit">
																<img src="<?=images_uri();?>ico_edit.png" alt="">
															</a>
															<a onclick="return confirm('Are you sure want to remove <?=$row['country_name'];?>?');" href="<?=base_url();?>country/delete/<?=$this->uri->slash_segment(2,'trailing').$row['country_id'];?>" data-toggle="tooltip" data-placement="top" title="">
																<img src="<?=images_uri();?>ico_del.png" alt="">
															</a>
														</td>
													</tr>
											<?php
												endforeach;
											endif;
											?>
												</tbody>
											</table>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
