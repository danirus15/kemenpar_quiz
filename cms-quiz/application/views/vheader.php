		<header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
			<div class="navbar-header dk">
				<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
					<i class="fa fa-bars"></i>
				</a>
				<a href="index.php" class="navbar-brand">
					<img src="<?=images_uri();?>logo.png" class="m-r-sm" alt="scale">
				</a>
				<a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
					<i class="fa fa-cog"></i>
				</a>
			</div>
			<ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello <?=$this->session->userdata('username');?> <b class="caret"></b></a>
					<ul class="dropdown-menu animated fadeInRight">
						<li>
							<a href="<?=base_url();?>logout">Logout</a>
						</li>
					</ul>
				</li>
			</ul>
		</header>
