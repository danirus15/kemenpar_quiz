<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">              
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Add/Edit Region</h3>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content --> 
									<section class="panel panel-default">
										<header class="panel-heading font-bold"></header>
										<div class="panel-body">
											<?=$form;?>
										</div>
									</section>
									<!-- e:content --> 
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
