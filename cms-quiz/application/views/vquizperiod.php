<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Quiz Period</h3>
											<a href="<?=base_url();?>quizperiod/add" class="btn btn-s-md btn-primary btn-rounded fr">Add</a>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<div class="table-responsive">
											<table class="table table-striped m-b-none" data-ride="datatables">
												<thead>
													<tr>
														<th width="20%">Time Period</th>
														<th width="20%">Region</th>
														<th width="30%">Template</th>
														<th width="10%">Status</th>
														<th width="10%">Action</th>
													</tr>
												</thead>
												<tbody>
											<?php
											if($totalData>0):
												foreach($itemData as $row):
											?>
													<tr>
														<td><?=$row['periode'];?></td>
														<td><?=$row['region_quiz'];?></td>
														<td><?=$row['template'];?></td>
														<td>
															<?php if($row['status']==1): ?>
															<img src="<?=images_uri();?>checked-2.png" style="height:25px;">
															<?php else: ?>
															<a href="<?=base_url();?>quizperiod/activated/<?=$row['id'];?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Non Active" data-original-title="Non Active" class="active">
																<img src="<?=images_uri();?>checked-1.png">
															<?php endif; ?>
															</a>
														</td>
														<td class="action">
															<a href="<?=base_url();?>quizperiod/edit/<?=$row['id'];?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit">
																<img src="<?=images_uri();?>ico_edit.png" alt="">
															</a>
															<a onclick="return confirm('Are you sure want to remove <?=$row['region_quiz'];?>?');" href="<?=base_url();?>quizperiod/delete/<?=$row['id'];?>" data-toggle="tooltip" data-placement="top" title="">
																<img src="<?=images_uri();?>ico_del.png" alt="">
															</a>
														</td>
													</tr>
											<?php
												endforeach;
											endif;
											?>
												</tbody>
											</table>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
