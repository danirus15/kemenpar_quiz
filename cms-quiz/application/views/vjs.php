	<script src="<?=js_uri();?>jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?=js_uri();?>bootstrap.js"></script>
	<!-- App -->
	<script src="<?=js_uri();?>app.js"></script>  
	<script src="<?=js_uri();?>slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?=js_uri();?>charts/easypiechart/jquery.easy-pie-chart.js"></script>
	<script src="<?=js_uri();?>charts/sparkline/jquery.sparkline.min.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.min.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.tooltip.min.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.spline.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.pie.min.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.resize.js"></script>
	<script src="<?=js_uri();?>charts/flot/jquery.flot.grow.js"></script>
	<script src="<?=js_uri();?>charts/flot/demo.js"></script>

	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="<?=js_uri();?>datatables/jquery.csv-0.71.min.js"></script>
	<script src="<?=js_uri();?>datatables/demo.js"></script>

	<script src="<?=js_uri();?>calendar/bootstrap_calendar.js"></script>
	<script src="<?=js_uri();?>calendar/demo.js"></script>
	<!-- datepicker -->
	<script src="<?=js_uri();?>datepicker/bootstrap-datepicker.js"></script>

	<!-- file input -->  
	<script src="<?=js_uri();?>file-input/bootstrap-filestyle.min.js"></script>

	<script src="<?=js_uri();?>wysiwyg/jquery.hotkeys.js"></script>
	<script src="<?=js_uri();?>wysiwyg/bootstrap-wysiwyg.js"></script>
	<script src="<?=js_uri();?>wysiwyg/demo.js"></script>

	<script src="<?=js_uri();?>chosen/chosen.jquery.min.js"></script>

	<script src="<?=js_uri();?>sortable/jquery.sortable.js"></script>
	<script src="<?=js_uri();?>app.plugin.js"></script>
	<script src="<?=js_uri();?>alertify.js"></script>

	<script type="text/javascript" language="javascript" src="<?=js_uri();?>tinymce/tinymce.min.js"></script>
	<script src="<?=js_uri();?>controller.js"></script>
</body>
</html>
