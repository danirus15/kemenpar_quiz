<!DOCTYPE html>
<html lang="en" class="app">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Wonderfull Indonesia</title>
	<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?=css_uri();?>bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>animate.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>icon.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>font.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>app.css" type="text/css" />
	<link rel="stylesheet" href="<?=css_uri();?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?=js_uri();?>calendar/bootstrap_calendar.css" type="text/css" />
	<link rel="stylesheet" href="<?=js_uri();?>datatables/datatables.css" type="text/css"/>
	<link rel="stylesheet" href="<?=js_uri();?>chosen/chosen.css" type="text/css" />
	<link rel="stylesheet" href="<?=js_uri();?>datepicker/datepicker.css" type="text/css" />

	<!--[if lt IE 9]>
	<script src="<?=js_uri();?>ie/html5shiv.js"></script>
	<script src="<?=js_uri();?>ie/respond.min.js"></script>
	<script src="<?=js_uri();?>ie/excanvas.js"></script>
	<![endif]-->
</head>
<body class="">
