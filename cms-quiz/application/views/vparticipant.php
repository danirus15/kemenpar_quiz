<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->

				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">              
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Participant Period 1</h3>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content --> 
									<section class="panel panel-default">
										<div class="table-responsive">
											<table class="table table_user table-striped m-b-none" data-ride="datatables2">
												<thead>
													<tr>
														<th>Total Score</th>
														<th>Quiz Score</th>
														<th>FB Score</th>
														<th>Tw Score</th>
														<th>Time</th>
														<th width="40%">Nama</th>
														<th width="20%">Email</th>
														<th>Country</th>
													</tr>
												</thead>
												<tbody>
											<?php
											//~ echo '<pre>'; print_r($itemData); die(' qwerty');
											if($totalData>0):
												foreach($itemData as $row):
											?>
													<tr>
														<td><?=$row['score_total'];?></td>
														<td><?=$row['score_quiz'];?></td>
														<td><?=$row['score_fb'];?></td>
														<td><?=$row['score_tw'];?></td>
														<td><?=substr($row['time'], 0,5);?></td>
														<td><?=$row['nama'];?></td>
														<td><?=$row['email'];?></td>
														<td><?=$row['country_name'];?></td>
													</tr>
											<?php
												endforeach;
											endif;
											?>
												</tbody>
											</table>
										</div>
									</section>
									<!-- e:content --> 
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
