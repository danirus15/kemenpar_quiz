<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Medium Question</h3>
											<a href="<?=base_url();?>qmedium/add" class="btn btn-s-md btn-primary btn-rounded fr">Add</a>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<div class="table-responsive">
											<table class="table table-striped m-b-none" data-ride="datatables">
												<thead>
													<tr>
														<th>Question</th>
														<th width="40%">Corect Answer</th>
														<th width="20%">Action</th>
													</tr>
												</thead>
												<tbody>
											<?php
											if($totalData>0):
												foreach($itemData as $row):
											?>
													<tr>
														<td><?=$row['question_en'];?></td>
														<td><?=$row['corect_answer'];?></td>
														<td class="action">
															<a href="<?=base_url();?>qmedium/edit/<?=$row['id_question'];?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit">
																<img src="<?=images_uri();?>ico_edit.png" alt="">
															</a>
															<a onclick="return confirm('Are you sure want to remove this item?');" href="<?=base_url();?>qmedium/delete/<?=$row['id_question'];?>" data-toggle="tooltip" data-placement="top" title="">
																<img src="<?=images_uri();?>ico_del.png" alt="">
															</a>
														</td>
													</tr>
											<?php
												endforeach;
											endif;
											?>
												</tbody>
											</table>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
