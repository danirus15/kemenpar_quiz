		<form action="<?=base_url();?>region/updatepost" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Region Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="region_name" value="<?=$itemData['region_name'];?>" required>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Language Default</label>
				<div class="col-sm-10">
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="en" name="default_lang" <?php if($itemData['default_lang']=='en') echo 'checked'; ?>>
							<i></i>
							English
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ar" name="default_lang" <?php if($itemData['default_lang']=='ar') echo 'checked'; ?>>
							<i></i>
							لعربية
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="fr" name="default_lang" <?php if($itemData['default_lang']=='fr') echo 'checked'; ?>>
							<i></i>
							Fran&ccedil;ais
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ja" name="default_lang" <?php if($itemData['default_lang']=='ja') echo 'checked'; ?>>
							<i></i>
							日本語
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ko" name="default_lang" <?php if($itemData['default_lang']=='ko') echo 'checked'; ?>>
							<i></i>
							한국어
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="zh-cn" name="default_lang" <?php if($itemData['default_lang']=='zh-cn') echo 'checked'; ?>>
							<i></i>
							简体中文
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="zh-tw" name="default_lang" <?php if($itemData['default_lang']=='zh-tw') echo 'checked'; ?>>
							<i></i>
							繁體中文
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<input type="hidden" name="region_id" value="<?=$itemData['region_id'];?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
