		<form action="<?=base_url();?>quizperiod/savepost" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label">Title Quiz</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="title_quiz" value="">
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Region</label>
				<div class="col-md-3">
					<select name="region_id" class="form-control" required>
						<option value="" selected>Choice</option>
				<?php
				if(count($regionData)>0):
					foreach($regionData as $row):
				?>
						<option value="<?=$row['region_id'];?>"><?=$row['region_name'];?></option>
				<?php
					endforeach;
				endif;
				?>
					</select>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Time Period</label>
				<div class="col-sm-10 date_pick">
					<input class="input-sm datepicker-input form-control" type="text" value="<?=date('d-m-Y');?>" data-date-format="dd-mm-yyyy" name="start"> - 
					<input class="input-sm datepicker-input form-control" type="text" value="<?=date('d-m-Y');?>" data-date-format="dd-mm-yyyy" name="end">
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Cover Image</label>
				<div class="col-sm-10">
					<label class="foto-form">
						<div id="imagePreview"></div>
						<input id="uploadFile" type="file" name="images" class="img" />
						<span>Upload Photo</span>
					</label>
					width: 1440x500px
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Template</label>
				<div class="col-sm-10">
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="natural" required>
							<i></i>
							Natural Wonders
						</label>
					</div>
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="cultural" required>
							<i></i>
							Cultural Wonders
						</label>
					</div>
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="adventurous" required>
							<i></i>
							Adventurous Wonders
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Number of Winner</label>
				<div class="col-md-3">
					<select name="num_of_winner" class="form-control" required>
						<option value="" selected>Choice</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<section class="panel panel-default">
				<header class="panel-heading bg-light">
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#english" data-toggle="tab">English</a></li>
						<li><a href="#arab" data-toggle="tab">العربية</a></li>
						<li><a href="#france" data-toggle="tab">Fran&ccedil;ais</a></li>
						<li><a href="#japan" data-toggle="tab">日本語</a></li>
						<li><a href="#korea" data-toggle="tab">한국어</a></li>
						<li><a href="#china" data-toggle="tab">简体中文</a></li>
						<li><a href="#taiwan" data-toggle="tab">繁體中文</a></li>
					</ul>
				</header>
				<div class="panel-body">
					<div class="tab-content">
						<!-- s:english -->
						<div class="tab-pane active" id="english">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_en" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_en" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:english -->
						<!-- s:arab -->
						<div class="tab-pane" id="arab">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ar" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ar" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:arab -->
						<!-- s:france -->
						<div class="tab-pane" id="france">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_fr" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_fr" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:france -->
						<!-- s:japan -->
						<div class="tab-pane" id="japan">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ja" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ja" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:japan -->
						<!-- s:korea -->
						<div class="tab-pane" id="korea">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ko" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ko" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:korea -->
						<!-- s:china -->
						<div class="tab-pane" id="china">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_zh-cn" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_zh-cn" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:china -->
						<!-- s:taiwan -->
						<div class="tab-pane" id="taiwan">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_zh-tw" value="">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_zh-tw" id="" rows="20"></textarea>
								</div>
							</div>
						</div>
						<!-- e:taiwan -->
					</div>
				</div>
			</section>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<input type="hidden" name="status" value="0">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
