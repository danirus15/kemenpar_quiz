		<form action="<?=base_url();?>name/savepost" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="nama" value="" required>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-10">
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="male" name="gender" required>
							<i></i>
							Male
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="female" name="gender" required>
							<i></i>
							Female
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
