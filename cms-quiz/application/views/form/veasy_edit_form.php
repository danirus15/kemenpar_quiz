	<form class="form-horizontal" method="post" action="<?=base_url();?>qeasy/updatepost">
		<div class="tab-content">
			<!-- s:english -->
			<div class="tab-pane active" id="english">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_en" value="<?=$itemData['question_en'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_en" value="<?=$itemData['answer_a_en'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_en" value="<?=$itemData['answer_b_en'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_en" value="<?=$itemData['answer_c_en'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_en" value="<?=$itemData['hints_en'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:english -->
			<!-- s:arab -->
			<div class="tab-pane" id="arab">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_ar" value="<?=$itemData['question_ar'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_ar" value="<?=$itemData['answer_a_ar'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_ar" value="<?=$itemData['answer_b_ar'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_ar" value="<?=$itemData['answer_c_ar'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_ar" value="<?=$itemData['hints_ar'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:arab -->
			<!-- s:france -->
			<div class="tab-pane" id="france">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_fr" value="<?=$itemData['question_fr'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_fr" value="<?=$itemData['answer_a_fr'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_fr" value="<?=$itemData['answer_b_fr'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_fr" value="<?=$itemData['answer_c_fr'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_fr" value="<?=$itemData['hints_fr'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:france -->
			<!-- s:japan -->
			<div class="tab-pane" id="japan">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_ja" value="<?=$itemData['question_ja'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_ja" value="<?=$itemData['answer_a_ja'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_ja" value="<?=$itemData['answer_b_ja'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_ja" value="<?=$itemData['answer_c_ja'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_ja" value="<?=$itemData['hints_ja'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:japan -->
			<!-- s:korea -->
			<div class="tab-pane" id="korea">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_ko" value="<?=$itemData['question_ko'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_ko" value="<?=$itemData['answer_a_ko'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_ko" value="<?=$itemData['answer_b_ko'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_ko" value="<?=$itemData['answer_c_ko'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_ko" value="<?=$itemData['hints_ko'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:korea -->
			<!-- s:china -->
			<div class="tab-pane" id="china">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_zh-cn" value="<?=$itemData['question_zh-cn'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_zh-cn" value="<?=$itemData['answer_a_zh-cn'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_zh-cn" value="<?=$itemData['answer_b_zh-cn'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_zh-cn" value="<?=$itemData['answer_c_zh-cn'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_zh-cn" value="<?=$itemData['hints_zh-cn'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:china -->
			<!-- s:taiwan -->
			<div class="tab-pane" id="taiwan">
				<div class="form-group">
					<label class="col-sm-2 control-label">Question</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="question_zh-tw" value="<?=$itemData['hints_zh-tw'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-10 answer">
						<div class="a1">A</div>
						<div class="a2"><input type="text" class="form-control" name="answer_a_zh-tw" value="<?=$itemData['hints_zh-tw'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">B</div>
						<div class="a2"><input type="text" class="form-control" name="answer_b_zh-tw" value="<?=$itemData['hints_zh-tw'];?>"></div>
						<div class="clearfix"></div>
						<div class="a1">C</div>
						<div class="a2"><input type="text" class="form-control" name="answer_c_zh-tw" value="<?=$itemData['hints_zh-tw'];?>"></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hints</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="hints_zh-tw" value="<?=$itemData['hints_zh-tw'];?>">
					</div>
				</div>
				<div class="line line-dashed b-b line-lg pull-in"></div>
			</div>
			<!-- e:taiwan -->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Corect Answer</label>
			<div class="col-sm-10">
				<label class="checkbox-inline">
					<input type="radio" id="inlineCheckbox1" name="corect_answer" value="A" <?php if($itemData['corect_answer']=='A') echo 'checked'; ?>> A
				</label>
				<label class="checkbox-inline">
					<input type="radio" id="inlineCheckbox1"  name="corect_answer" value="B" <?php if($itemData['corect_answer']=='B') echo 'checked'; ?>> B
				</label>
				<label class="checkbox-inline">
					<input type="radio" id="inlineCheckbox1" name="corect_answer" value="C" <?php if($itemData['corect_answer']=='C') echo 'checked'; ?>> C
				</label>
			</div>
		</div>
		<div class="line line-dashed b-b line-lg pull-in"></div>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				<input type="hidden" name="id_question" value="<?=$itemData['id_question'];?>">
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</div>
	</form>
