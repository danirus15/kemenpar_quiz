<?php
$timeperiod = explode('|',$itemData['periode']);
?>
		<form action="<?=base_url();?>quizperiod/updatepost" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label">Title Quiz</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="title_quiz" value="<?=$itemData['region_quiz'];?>">
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Region</label>
				<div class="col-md-3">
					<select name="region_id" class="form-control">
				<?php
				if(count($regionData)>0):
					foreach($regionData as $row):
				?>
						<option value="<?=$row['region_id'];?>" <?php if($row['region_id']==$itemData['region_id']) echo 'selected'; ?>><?=$row['region_name'];?></option>
				<?php
					endforeach;
				endif;
				?>
					</select>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Time Period</label>
				<div class="col-sm-10 date_pick">
					<input class="input-sm datepicker-input form-control" type="text" value="<?=$timeperiod[0];?>" data-date-format="dd-mm-yyyy" name="start"> - 
					<input class="input-sm datepicker-input form-control" type="text" value="<?=$timeperiod[1];?>" data-date-format="dd-mm-yyyy" name="end">
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Cover Image</label>
				<div class="col-sm-10">
					<label class="foto-form">
						<div id="imagePreview" style="background:url(<?=images_content().$itemData['image_cover'];?>); background-size:cover;"></div>
						<input id="uploadFile" type="file" name="images" class="img" />
						<input type="hidden" name="old_images" value="<?=$itemData['image_cover'];?>">
						<span>Upload Photo</span>
					</label>
					width: 1440x500px
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Template</label>
				<div class="col-sm-10">
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="natural" <?php if($itemData['template']=='natural') echo 'checked'; ?>>
							<i></i>
							Natural Wonders
						</label>
					</div>
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="cultural" <?php if($itemData['template']=='cultural') echo 'checked'; ?>>
							<i></i>
							Cultural Wonders
						</label>
					</div>
					<div class="radio i-checks">
						<label>
							<input type="radio" name="template" value="adventurous" <?php if($itemData['template']=='adventurous') echo 'checked'; ?>>
							<i></i>
							Adventurous Wonders
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Number of Winner</label>
				<div class="col-md-3">
					<select name="num_of_winner" class="form-control">
						<option value="" selected>Choice</option>
						<option value="1" <?php if($itemData['num_of_winner']==1) echo 'selected'; ?>>1</option>
						<option value="2" <?php if($itemData['num_of_winner']==2) echo 'selected'; ?>>2</option>
						<option value="3" <?php if($itemData['num_of_winner']==3) echo 'selected'; ?>>3</option>
						<option value="4" <?php if($itemData['num_of_winner']==4) echo 'selected'; ?>>4</option>
						<option value="5" <?php if($itemData['num_of_winner']==5) echo 'selected'; ?>>5</option>
						<option value="6" <?php if($itemData['num_of_winner']==6) echo 'selected'; ?>>6</option>
						<option value="7" <?php if($itemData['num_of_winner']==7) echo 'selected'; ?>>7</option>
						<option value="8" <?php if($itemData['num_of_winner']==8) echo 'selected'; ?>>8</option>
						<option value="9" <?php if($itemData['num_of_winner']==9) echo 'selected'; ?>>9</option>
						<option value="10" <?php if($itemData['num_of_winner']==10) echo 'selected'; ?>>10</option>
					</select>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<section class="panel panel-default">
				<header class="panel-heading bg-light">
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#english" data-toggle="tab">English</a></li>
						<li><a href="#arab" data-toggle="tab">العربية</a></li>
						<li><a href="#france" data-toggle="tab">Fran&ccedil;ais</a></li>
						<li><a href="#japan" data-toggle="tab">日本語</a></li>
						<li><a href="#korea" data-toggle="tab">한국어</a></li>
						<li><a href="#china" data-toggle="tab">简体中文</a></li>
						<li><a href="#taiwan" data-toggle="tab">繁體中文</a></li>
					</ul>
				</header>
				<div class="panel-body">
					<div class="tab-content">
						<!-- s:english -->
						<div class="tab-pane active" id="english">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_en" value="<?=$itemData['title_en'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_en" id="" rows="20"><?=$itemData['description_en'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:english -->
						<!-- s:arab -->
						<div class="tab-pane" id="arab">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ar" value="<?=$itemData['title_ar'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ar" id="" rows="20"><?=$itemData['description_ar'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:arab -->
						<!-- s:france -->
						<div class="tab-pane" id="france">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_fr" value="<?=$itemData['title_fr'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_fr" id="" rows="20"><?=$itemData['description_fr'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:france -->
						<!-- s:japan -->
						<div class="tab-pane" id="japan">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ja" value="<?=$itemData['title_ja'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ja" id="" rows="20"><?=$itemData['description_ja'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:japan -->
						<!-- s:korea -->
						<div class="tab-pane" id="korea">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_ko" value="<?=$itemData['title_ko'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_ko" id="" rows="20"><?=$itemData['description_ko'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:korea -->
						<!-- s:china -->
						<div class="tab-pane" id="china">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_zh-cn" value="<?=$itemData['title_zh-cn'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_zh-cn" id="" rows="20"><?=$itemData['description_zh-cn'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:china -->
						<!-- s:taiwan -->
						<div class="tab-pane" id="taiwan">
							<div class="form-group">
								<label class="col-sm-2 control-label">Title</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="title_zh-tw" value="<?=$itemData['title_zh-tw'];?>">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg pull-in"></div>
							<div class="clearfix mt20"></div>
							<div class="form-group ">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea class="input-sm form-control" name="description_zh-tw" id="" rows="20"><?=$itemData['description_zh-tw'];?></textarea>
								</div>
							</div>
						</div>
						<!-- e:taiwan -->
					</div>
				</div>
			</section>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<input type="hidden" name="id" value="<?=$itemData['id'];?>">
					<input type="hidden" name="status" value="<?=$itemData['status'];?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
