		<form action="<?=base_url();?>country/savepost" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Country Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="country_name" value="" required>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">IP Address</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="ip" value="" required>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Language Default</label>
				<div class="col-sm-10">
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="en" name="def_lang" required>
							<i></i>
							English
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ar" name="def_lang" required>
							<i></i>
							لعربية
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="fr" name="def_lang" required>
							<i></i>
							Fran&ccedil;ais
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ja" name="def_lang" required>
							<i></i>
							日本語
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="ko" name="def_lang" required>
							<i></i>
							한국어
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="zh-cn" name="def_lang" required>
							<i></i>
							简体中文
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="zh-tw" name="def_lang" required>
							<i></i>
							繁體中文
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<input type="hidden" name="region_id" value="<?=$this->uri->segment(3);?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
