		<form action="<?=base_url();?>name/updatepost" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="nama" value="<?=$itemData['nama'];?>" required>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-10">
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="male" name="gender" <?php if($itemData['gender']=='male') echo 'checked'; ?>>
							<i></i>
							Male
						</label>
					</div>
					<div class="checkbox i-checks">
						<label>
							<input type="radio" value="female" name="gender" <?php if($itemData['gender']=='female') echo 'checked'; ?>>
							<i></i>
							Female
						</label>
					</div>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg pull-in"></div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-2">
					<input type="hidden" name="id" value="<?=$itemData['id'];?>">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
