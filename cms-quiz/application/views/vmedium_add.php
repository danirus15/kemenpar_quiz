<?=$head;?>
	<section class="vbox">
		<?=$header;?>
		<section>
			<section class="hbox stretch">
				<!-- .aside -->
				<?=$menu;?>
				<!-- /.aside -->
				<section id="content">
					<section class="hbox stretch">
						<section>
							<section class="vbox">
								<section class="scrollable padder">
									<section class="row m-b-md">
										<div class="title_page">
											<h3 class="m-b-xs text-black fl">Add/Edit Question Medium</h3>
											<div class="clearfix"></div>
										</div>
									</section>
									<div class="clearfix"></div>
									<!-- s:content -->
									<section class="panel panel-default">
										<header class="panel-heading bg-light">
											<ul class="nav nav-tabs nav-justified">
												<li class="active"><a href="#english" data-toggle="tab">English</a></li>
												<li><a href="#arab" data-toggle="tab">العربية</a></li>
												<li><a href="#france" data-toggle="tab">Fran&ccedil;ais</a></li>
												<li><a href="#japan" data-toggle="tab">日本語</a></li>
												<li><a href="#korea" data-toggle="tab">한국어</a></li>
												<li><a href="#china" data-toggle="tab">简体中文</a></li>
												<li><a href="#taiwan" data-toggle="tab">繁體中文</a></li>
											</ul>
										</header>
										<div class="panel-body">
											<?=$form;?>
										</div>
									</section>
									<!-- e:content -->
								</section>
							</section>
						</section>
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
				</section>
			</section>
		</section>
	</section>
	<?=$vjs;?>
