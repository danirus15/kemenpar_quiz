<?php
function language($language){
    switch($language)
    {
		case 'en':
			$lang = "English";
			break;
		case 'ar':
			$lang = "لعربية";
			break;
		case 'fr':
			$lang = "Fran&ccedil;ais";
			break;
		case 'ja':
			$lang = "日本語";
			break;
		case 'ko':
			$lang = "한국어";
			break;
		case 'zh-cn':
			$lang = "简体中文";
			break;
		case 'zh-tw':
			$lang = "繁體中文";
			break;
	}
    return $lang;
}
?>
