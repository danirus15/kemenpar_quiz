<?php
function renameFile($str)
{
	$str = strtolower(str_replace(" ", "-", $str));
	//~ $str = substr_replace($str, uniqid(rand()), 0, 10);
	return $str;
}

function NewGuid()
{
	$s = strtolower(md5(uniqid(rand(), true)));
	$guidText = substr($s, 0, 8) . '-' . substr($s, 8, 4);
	return $guidText;
}

function findFileExtension($filename)
{
	$filename = strtolower($filename);
	$exts = preg_split("[\.]", $filename);
	#$exts = split("[/\\.]", $filename);
	$n = count($exts) - 1;
	$exts = $exts[$n];
	return $exts;
}

function string_limit_words($string, $word_limit)
{
	$words = explode(' ', $string);
	return implode(' ', array_slice($words, 0, $word_limit));
}

function slug($string, $delimiter = '-')
{
	$new_string = strip_tags($string);
	$new_string = strtolower($new_string);
	$new_string = preg_replace("/&(.)(uml);/", "$1e", $new_string);
	$new_string = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $new_string);
	$new_string = preg_replace("/([^a-z0-9]+)/", $delimiter, $new_string);
	$new_string = trim($new_string, $delimiter);
	return $new_string;
}

?>
