<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {
    var $tableName = "admin";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}

    function getAllItems()
    {
        $this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->order_by("idAdmin", "DESC");
        $query = $this->db->get();
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getItemById($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("idAdmin", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getLimitedItems($offset, $limit)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->order_by("idAdmin", "DESC");
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function countAllData()
    {
		return $this->db->count_all_results($this->tableName);
    }

    function addNewItem($data)
    {
        $this->db->insert($this->tableName, $data);
    }

    function updateItemById($data, $id)
    {
        $this->db->update($this->tableName, $data, array('idAdmin' => $id));
    }

    function deleteItemById($id)
    {
        $this->db->where('idAdmin', $id);
		$this->db->delete($this->tableName);
		return true;
    }

}
