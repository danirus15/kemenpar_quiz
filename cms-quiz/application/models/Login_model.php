<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function cekLogin($username,$passwd)
	{
		$query = $this->db->get_where('admin', array('username' => $username,'passwd' => $passwd));

		if ($query->num_rows() == 1){
			$res = $query->row();
			$query->free_result();

			$this->session->set_userdata('username', $res->username);

			return TRUE;
		}
		else{
			$this->session->set_flashdata('erroLogin', 'username dan / atau kata sandi tidak cocok, silahkan coba lagi atau hubungi administrator sistem Anda.');
			return FALSE;
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		return TRUE;
	}

}
