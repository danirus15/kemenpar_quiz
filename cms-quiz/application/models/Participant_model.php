<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_model extends CI_Model {

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}

    function getDataById($id)
    {
        $q = "SELECT s.*, u.nama, u.email, c.country_name 
				FROM quizperiod q 
				INNER JOIN scores s on q.id=s.id_period 
				INNER JOIN users u on s.user_id=u.user_id 
				INNER JOIN country c on u.country_id=c.country_id 
				WHERE q.id=".$id."
				ORDER BY s.score_total DESC";
		$query = $this->db->query($q);
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function countDataById($id)
    {
        $q = "SELECT s.*, u.nama, u.email, c.country_name 
				FROM quizperiod q 
				INNER JOIN scores s on q.id=s.id_period 
				INNER JOIN users u on s.user_id=u.user_id 
				INNER JOIN country c on u.country_id=c.country_id 
				WHERE q.id=".$id."
				ORDER BY s.score_total DESC";
		$this->db->query($q);
		return $this->db->count_all_results();
    }

}
