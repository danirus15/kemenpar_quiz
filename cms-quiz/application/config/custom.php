<?php
// path and directory
$config['doc_root'] 		= 'http://' . $_SERVER['HTTP_HOST'] . '/quiz/';
$config['root_dir'] 		= realpath(dirname(__FILE__)."/../../../")."/";
$config['data_dir'] 		= $config['root_dir'] . 'xdata/';

$config['tmp_images_dir']	= $config['data_dir'] . 'tmp/';
$config['images_dir']		= $config['data_dir'] . 'images/';

$config['table_total_rows']	= 10;

$config['template_data']	= $config['doc_root'] .'xdata/';

$config['template_uri']		= $config['doc_root'] .'cms-quiz/public/';
$config['css_uri']			= $config['template_uri'] .'css/';
$config['js_uri']			= $config['template_uri'] .'js/';
$config['fonts_uri']		= $config['template_uri'] .'fonts/';
$config['images_uri']		= $config['template_uri'] .'images/';

$config['images_content']	= $config['template_data'] .'images/';
?>
