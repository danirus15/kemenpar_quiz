<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 30/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Country extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('country_model');
		$this->load->model('region_model');
		$this->load->model('period_model');
		$this->load->helper('language');
	}

	public function index($region_id)
	{
		$totalRecord			= $this->country_model->countAllDataByRegionId($region_id);
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->country_model->getAllItemsByRegionId($region_id);
		//~ echo '<pre>'; print_r($this->db->last_query()); die(' qwerty');

		$contents['dataRegion']	= $this->region_model->getItemById($region_id);

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vcountry',$contents);
	}

	public function add($region_id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["form"] = $this->load->view('form/vcountry_add_form',null,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vcountry_add',$contents);
	}

	function savepost()
	{
		$data['country_name'] 	= $this->input->post('country_name');
		$data['ip'] 			= $this->input->post('ip');
		$data['def_lang'] 	= $this->input->post('def_lang');
		$data['region_id'] 		= $this->input->post('region_id');

		$this->country_model->addNewItem($data);
		$totalRecord = $this->country_model->countAllDataByRegionId($data['region_id']);
		if($totalRecord==1){
			$country = 'country';
		}
		elseif($totalRecord>1){
			$country = 'countries';
		}
		else{
			$country = '';
		}
		$dataRegion['country'] = $totalRecord.' '.$country;
		$this->region_model->updateItemById($dataRegion,$data['region_id']);
		redirect('country/'.$data['region_id']);
	}

	public function edit($region_id,$id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['itemData'] = $this->country_model->getItemById($id);
		$contents["form"] = $this->load->view('form/vcountry_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vcountry_add',$contents);
	}

	function updatepost()
	{
		$data['country_id'] 	= $this->input->post('country_id');
		$data['country_name'] 	= $this->input->post('country_name');
		$data['ip'] 			= $this->input->post('ip');
		$data['def_lang'] 	= $this->input->post('def_lang');
		$data['region_id'] 		= $this->input->post('region_id');

        $this->country_model->updateItemById($data, $data['country_id']);
		redirect('country/'.$data['region_id']);
	}

	function delete($region_id,$id)
	{
		$is_delete = $this->country_model->deleteItemById($id);
		if ($is_delete) {
			$totalRecord = $this->country_model->countAllDataByRegionId($region_id);
			if($totalRecord==1){
				$country = 'country';
			}
			elseif($totalRecord>1){
				$country = 'countries';
			}
			else{
				$country = '';
			}
			$dataRegion['country'] = $totalRecord.' '.$country;
			$this->region_model->updateItemById($dataRegion,$region_id);
			redirect('country/'.$region_id);
		} else {
			echo '<script>alert("Failed to removed");</script>';
		}
	}

}
