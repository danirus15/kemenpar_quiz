<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 14/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Howto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('howto_model');
	}

	public function index($id='')
	{
		$this->load->model('period_model');
		$id = $this->uri->segment(2,1);

		$contents['itemData']	= $this->howto_model->getItemById($id);

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'statik';
		$m_data["page"] = 'howto'.$id;
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vhowto',$contents);
	}

	function updatepost()
	{
		header('Content-Type: text/html; charset=utf-8');

		$data['id'] 				= $this->input->post('id');
		$data['title_en'] 			= $this->input->post('title_en');
		$data['title_ar'] 			= $this->input->post('title_ar');
		$data['title_fr'] 			= $this->input->post('title_fr');
		$data['title_ja'] 			= $this->input->post('title_ja');
		$data['title_ko'] 			= $this->input->post('title_ko');
		$data['title_zh-cn'] 		= $this->input->post('title_zh-cn');
		$data['title_zh-tw'] 		= $this->input->post('title_zh-tw');
		$data['description_en'] 	= $this->input->post('description_en');
		$data['description_ar'] 	= $this->input->post('description_ar');
		$data['description_fr'] 	= $this->input->post('description_fr');
		$data['description_ja'] 	= $this->input->post('description_ja');
		$data['description_ko'] 	= $this->input->post('description_ko');
		$data['description_zh-cn'] 	= $this->input->post('description_zh-cn');
		$data['description_zh-tw'] 	= $this->input->post('description_zh-tw');
		$data['step1_en'] 			= $this->input->post('step1_en');
		$data['step1_ar'] 			= $this->input->post('step1_ar');
		$data['step1_fr'] 			= $this->input->post('step1_fr');
		$data['step1_ja'] 			= $this->input->post('step1_ja');
		$data['step1_ko'] 			= $this->input->post('step1_ko');
		$data['step1_zh-cn'] 		= $this->input->post('step1_zh-cn');
		$data['step1_zh-tw'] 		= $this->input->post('step1_zh-tw');
		$data['step2_en'] 			= $this->input->post('step2_en');
		$data['step2_ar'] 			= $this->input->post('step2_ar');
		$data['step2_fr'] 			= $this->input->post('step2_fr');
		$data['step2_ja'] 			= $this->input->post('step2_ja');
		$data['step2_ko'] 			= $this->input->post('step2_ko');
		$data['step2_zh-cn'] 		= $this->input->post('step2_zh-cn');
		$data['step2_zh-tw'] 		= $this->input->post('step2_zh-tw');
		$data['step3_en'] 			= $this->input->post('step3_en');
		$data['step3_ar'] 			= $this->input->post('step3_ar');
		$data['step3_fr'] 			= $this->input->post('step3_fr');
		$data['step3_ja'] 			= $this->input->post('step3_ja');
		$data['step3_ko'] 			= $this->input->post('step3_ko');
		$data['step3_zh-cn'] 		= $this->input->post('step3_zh-cn');
		$data['step3_zh-tw'] 		= $this->input->post('step3_zh-tw');
		$data['title_prize_en'] 	= $this->input->post('title_prize_en');
		$data['title_prize_ar'] 	= $this->input->post('title_prize_ar');
		$data['title_prize_fr'] 	= $this->input->post('title_prize_fr');
		$data['title_prize_ja'] 	= $this->input->post('title_prize_ja');
		$data['title_prize_ko'] 	= $this->input->post('title_prize_ko');
		$data['title_prize_zh-cn'] 	= $this->input->post('title_prize_zh-cn');
		$data['title_prize_zh-tw'] 	= $this->input->post('title_prize_zh-tw');
		$data['desc_prize_en'] 		= $this->input->post('desc_prize_en');
		$data['desc_prize_ar'] 		= $this->input->post('desc_prize_ar');
		$data['desc_prize_fr'] 		= $this->input->post('desc_prize_fr');
		$data['desc_prize_ja'] 		= $this->input->post('desc_prize_ja');
		$data['desc_prize_ko'] 		= $this->input->post('desc_prize_ko');
		$data['desc_prize_zh-cn'] 	= $this->input->post('desc_prize_zh-cn');
		$data['desc_prize_zh-tw'] 	= $this->input->post('desc_prize_zh-tw');

		$this->howto_model->updateItemById($data, $data['id']);
		redirect('howto/'.$data['id']);
	}

}
