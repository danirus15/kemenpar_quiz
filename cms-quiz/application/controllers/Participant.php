<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Participant extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('participant_model');
		$this->load->model('period_model');
		$this->load->helper('language');
	}

	public function index($id='')
	{
		$id = $this->uri->segment(2,1);

		$totalRecord			= $this->participant_model->countDataById($id);
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->participant_model->getDataById($id);

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'participant';
		$m_data["page"] = $id;
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vparticipant',$contents);
	}

}
