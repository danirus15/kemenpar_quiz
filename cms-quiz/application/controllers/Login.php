<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		if($this->session->userdata('username')!=''){
			//~ redirect('dashboard');
			redirect('quizperiod');
		}
		else{
			$this->load->view('vlogin');
		}
	}

	public function cekLogin()
	{
		$username = $this->input->post('username');
		$passwd = md5($this->input->post('passwd'));

		if($this->login_model->cekLogin($username,$passwd)){
			//~ redirect('dashboard');
			redirect('quizperiod');
		}
		else{
			redirect('login');
		}
	}

	public function dashboard()
	{
		if($this->session->userdata('username')==''){
			redirect('login');
		}
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);
		$m_data["pages"] = '';
		$m_data['page'] = 'dashboard';
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vdashboard',$contents);
	}

	public function logout()
	{
		if($this->login_model->logout()){
			redirect('login');
		}
	}

}
