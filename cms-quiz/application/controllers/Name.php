<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Name extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('name_model');
		$this->load->model('period_model');
	}

	public function index()
	{
		$totalRecord			= $this->name_model->countAllData();
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->name_model->getAllItems();

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'name';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vname',$contents);
	}

	public function add()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'name';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["form"] = $this->load->view('form/vname_add_form',null,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vname_add',$contents);
	}

	function savepost()
	{
		$data['nama'] 		= $this->input->post('nama');
		$data['gender'] 	= $this->input->post('gender');

		$this->name_model->addNewItem($data);
		redirect('name');
	}

	public function edit($id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'name';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['itemData'] = $this->name_model->getItemById($id);
		$contents["form"] = $this->load->view('form/vname_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vname_add',$contents);
	}

	function updatepost()
	{
		$data['id'] 		= $this->input->post('id');
		$data['nama'] 		= $this->input->post('nama');
		$data['gender'] 	= $this->input->post('gender');

        $this->name_model->updateItemById($data, $data['id']);
		redirect('name');
	}

	function delete($id)
	{
		$is_delete = $this->name_model->deleteItemById($id);
		if ($is_delete) {
			redirect('name');
		} else {
			echo '<script>alert("Failed to removed");</script>';
		}
	}

}
