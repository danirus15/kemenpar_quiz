<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Term extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('term_model');
	}

	public function index($id='')
	{
		$this->load->model('period_model');
		$id = $this->uri->segment(2,1);

		$contents['itemData']	= $this->term_model->getItemById($id);

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'statik';
		$m_data["page"] = 'term'.$id;
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vterm',$contents);
	}

	function updatepost()
	{
		header('Content-Type: text/html; charset=utf-8');

		$data['id'] 				= $this->input->post('id');
		$data['title_en'] 			= $this->input->post('title_en');
		$data['title_ar'] 			= $this->input->post('title_ar');
		$data['title_fr'] 			= $this->input->post('title_fr');
		$data['title_ja'] 			= $this->input->post('title_ja');
		$data['title_ko'] 			= $this->input->post('title_ko');
		$data['title_zh-cn'] 		= $this->input->post('title_zh-cn');
		$data['title_zh-tw'] 		= $this->input->post('title_zh-tw');
		$data['description_en'] 	= $this->input->post('description_en');
		$data['description_ar'] 	= $this->input->post('description_ar');
		$data['description_fr'] 	= $this->input->post('description_fr');
		$data['description_ja'] 	= $this->input->post('description_ja');
		$data['description_ko'] 	= $this->input->post('description_ko');
		$data['description_zh-cn'] 	= $this->input->post('description_zh-cn');
		$data['description_zh-tw'] 	= $this->input->post('description_zh-tw');

		$this->term_model->updateItemById($data, $data['id']);
		redirect('term/'.$data['id']);
	}

}
