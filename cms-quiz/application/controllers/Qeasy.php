<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 01/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Qeasy extends CI_Controller {
	var $bobot = 200;
	var $level = 1;

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('questions_model');
		$this->load->model('period_model');
	}

	public function index()
	{
		$totalRecord			= $this->questions_model->countAllData();
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->questions_model->getAllItems($this->level);

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'question';
		$m_data["page"] = 'easy';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('veasy',$contents);
	}

	public function add()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'question';
		$m_data["page"] = 'easy';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["form"] = $this->load->view('form/veasy_add_form',null,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('veasy_add',$contents);
	}

	function savepost()
	{
		$data['question_en'] 	= $this->input->post('question_en');
		$data['question_ar'] 	= $this->input->post('question_ar');
		$data['question_fr'] 	= $this->input->post('question_fr');
		$data['question_ja'] 	= $this->input->post('question_ja');
		$data['question_ko'] 	= $this->input->post('question_ko');
		$data['question_zh-cn'] = $this->input->post('question_zh-cn');
		$data['question_zh-tw'] = $this->input->post('question_zh-tw');
		$data['answer_a_en'] 	= $this->input->post('answer_a_en');
		$data['answer_a_ar'] 	= $this->input->post('answer_a_ar');
		$data['answer_a_fr'] 	= $this->input->post('answer_a_fr');
		$data['answer_a_ja'] 	= $this->input->post('answer_a_ja');
		$data['answer_a_ko'] 	= $this->input->post('answer_a_ko');
		$data['answer_a_zh-cn'] = $this->input->post('answer_a_zh-cn');
		$data['answer_a_zh-tw'] = $this->input->post('answer_a_zh-tw');
		$data['answer_b_en'] 	= $this->input->post('answer_b_en');
		$data['answer_b_ar'] 	= $this->input->post('answer_b_ar');
		$data['answer_b_fr'] 	= $this->input->post('answer_b_fr');
		$data['answer_b_ja'] 	= $this->input->post('answer_b_ja');
		$data['answer_b_ko'] 	= $this->input->post('answer_b_ko');
		$data['answer_b_zh-cn'] = $this->input->post('answer_b_zh-cn');
		$data['answer_b_zh-tw'] = $this->input->post('answer_b_zh-tw');
		$data['answer_c_en'] 	= $this->input->post('answer_c_en');
		$data['answer_c_ar'] 	= $this->input->post('answer_c_ar');
		$data['answer_c_fr'] 	= $this->input->post('answer_c_fr');
		$data['answer_c_ja'] 	= $this->input->post('answer_c_ja');
		$data['answer_c_ko'] 	= $this->input->post('answer_c_ko');
		$data['answer_c_zh-cn'] = $this->input->post('answer_c_zh-cn');
		$data['answer_c_zh-tw'] = $this->input->post('answer_c_zh-tw');
		$data['hints_en'] 		= $this->input->post('hints_en');
		$data['hints_ar'] 		= $this->input->post('hints_ar');
		$data['hints_fr'] 		= $this->input->post('hints_fr');
		$data['hints_ja'] 		= $this->input->post('hints_ja');
		$data['hints_ko'] 		= $this->input->post('hints_ko');
		$data['hints_zh-cn'] 	= $this->input->post('hints_zh-cn');
		$data['hints_zh-tw'] 	= $this->input->post('hints_zh-tw');
		$data['corect_answer'] 	= $this->input->post('corect_answer');
		$data['weight'] 		= $this->bobot;
		$data['level'] 			= $this->level;

		$this->questions_model->addNewItem($data);
		redirect('qeasy');
	}

	public function edit($id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = 'question';
		$m_data["page"] = 'easy';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['itemData'] = $this->questions_model->getItemById($id);
		$contents["form"] = $this->load->view('form/veasy_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('veasy_add',$contents);
	}

	function updatepost()
	{
		$data['id_question'] 	= $this->input->post('id_question');
		$data['question_en'] 	= $this->input->post('question_en');
		$data['question_ar'] 	= $this->input->post('question_ar');
		$data['question_fr'] 	= $this->input->post('question_fr');
		$data['question_ja'] 	= $this->input->post('question_ja');
		$data['question_ko'] 	= $this->input->post('question_ko');
		$data['question_zh-cn'] = $this->input->post('question_zh-cn');
		$data['question_zh-tw'] = $this->input->post('question_zh-tw');
		$data['answer_a_en'] 	= $this->input->post('answer_a_en');
		$data['answer_a_ar'] 	= $this->input->post('answer_a_ar');
		$data['answer_a_fr'] 	= $this->input->post('answer_a_fr');
		$data['answer_a_ja'] 	= $this->input->post('answer_a_ja');
		$data['answer_a_ko'] 	= $this->input->post('answer_a_ko');
		$data['answer_a_zh-cn'] = $this->input->post('answer_a_zh-cn');
		$data['answer_a_zh-tw'] = $this->input->post('answer_a_zh-tw');
		$data['answer_b_en'] 	= $this->input->post('answer_b_en');
		$data['answer_b_ar'] 	= $this->input->post('answer_b_ar');
		$data['answer_b_fr'] 	= $this->input->post('answer_b_fr');
		$data['answer_b_ja'] 	= $this->input->post('answer_b_ja');
		$data['answer_b_ko'] 	= $this->input->post('answer_b_ko');
		$data['answer_b_zh-cn'] = $this->input->post('answer_b_zh-cn');
		$data['answer_b_zh-tw'] = $this->input->post('answer_b_zh-tw');
		$data['answer_c_en'] 	= $this->input->post('answer_c_en');
		$data['answer_c_ar'] 	= $this->input->post('answer_c_ar');
		$data['answer_c_fr'] 	= $this->input->post('answer_c_fr');
		$data['answer_c_ja'] 	= $this->input->post('answer_c_ja');
		$data['answer_c_ko'] 	= $this->input->post('answer_c_ko');
		$data['answer_c_zh-cn'] = $this->input->post('answer_c_zh-cn');
		$data['answer_c_zh-tw'] = $this->input->post('answer_c_zh-tw');
		$data['hints_en'] 		= $this->input->post('hints_en');
		$data['hints_ar'] 		= $this->input->post('hints_ar');
		$data['hints_fr'] 		= $this->input->post('hints_fr');
		$data['hints_ja'] 		= $this->input->post('hints_ja');
		$data['hints_ko'] 		= $this->input->post('hints_ko');
		$data['hints_zh-cn'] 	= $this->input->post('hints_zh-cn');
		$data['hints_zh-tw'] 	= $this->input->post('hints_zh-tw');
		$data['corect_answer'] 	= $this->input->post('corect_answer');
		$data['weight'] 		= $this->bobot;
		$data['level'] 			= $this->level;

        $this->questions_model->updateItemById($data, $data['id_question']);
		redirect('qeasy');
	}

	function delete($id)
	{
		$is_delete = $this->questions_model->deleteItemById($id);
		if ($is_delete) {
			redirect('qeasy');
		} else {
			echo '<script>alert("Failed to removed");</script>';
		}
	}

}
