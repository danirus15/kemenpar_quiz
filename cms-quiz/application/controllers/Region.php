<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Region extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('region_model');
		$this->load->model('period_model');
		$this->load->helper('language');
	}

	public function index()
	{
		$totalRecord			= $this->region_model->countAllData();
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->region_model->getAllItems();

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vregion',$contents);
	}

	public function add()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["form"] = $this->load->view('form/vregion_add_form',null,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vregion_add',$contents);
	}

	function savepost()
	{
		$data['region_name'] 	= $this->input->post('region_name');
		$data['default_lang'] 	= $this->input->post('default_lang');

		$this->region_model->addNewItem($data);
		redirect('region');
	}

	public function edit($id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'region';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['itemData'] = $this->region_model->getItemById($id);
		$contents["form"] = $this->load->view('form/vregion_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vregion_add',$contents);
	}

	function updatepost()
	{
		$data['region_id'] 		= $this->input->post('region_id');
		$data['region_name'] 	= $this->input->post('region_name');
		$data['default_lang'] 	= $this->input->post('default_lang');

        $this->region_model->updateItemById($data, $data['region_id']);
		redirect('region');
	}

	function delete($id)
	{
		$is_delete = $this->region_model->deleteItemById($id);
		if ($is_delete) {
			redirect('region');
		} else {
			echo '<script>alert("Failed to removed");</script>';
		}
	}

}
