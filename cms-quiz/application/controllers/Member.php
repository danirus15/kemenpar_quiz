<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 29/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Member extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('member_model');
		$this->load->model('period_model');
	}

	public function index()
	{
		$totalRecord			= $this->member_model->countAllData();
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->member_model->getAllItems();

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'member';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vmember',$contents);
	}

	public function add()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'member';
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["form"] = $this->load->view('form/vmember_add_form',null,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vmember_add',$contents);
	}

	function savepost()
	{
		$data['username'] 		= $this->input->post('username');
		$data['passwd'] 		= md5($this->input->post('passwd'));

		$this->member_model->addNewItem($data);
		redirect('member');
	}

	public function edit($id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'member';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['itemData'] = $this->member_model->getItemById($id);
		$contents["form"] = $this->load->view('form/vmember_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vmember_add',$contents);
	}

	function updatepost()
	{
		$data['idAdmin'] 		= $this->input->post('idAdmin');
		$data['username'] 		= $this->input->post('username');
		$data['passwd'] 		= md5($this->input->post('passwd'));

        $this->member_model->updateItemById($data, $data['idAdmin']);
		redirect('member');
	}

	function delete($id)
	{
		$is_delete = $this->member_model->deleteItemById($id);
		if ($is_delete) {
			redirect('member');
		} else {
			echo '<script>alert("Failed to removed");</script>';
		}
	}

}
