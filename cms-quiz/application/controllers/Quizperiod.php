<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 03/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Quizperiod extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('username')=='') redirect('login');
		$this->load->model('period_model');
		$this->load->model('region_model');
		$this->load->helper('quickstuff');
	}

	public function index()
	{
		$totalRecord			= $this->period_model->countAllData();
		$contents['totalData']	= $totalRecord;
		$contents['itemData']	= $this->period_model->getAllItems();

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'quiz';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vquizperiod',$contents);
	}

	public function add()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'quiz';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$data['regionData']	= $this->region_model->getAllItems();
		$contents["form"] = $this->load->view('form/vquizperiod_add_form',$data,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vquizperiod_add',$contents);
	}

	function savepost()
	{
		$data['region_quiz'] 		= $this->input->post('title_quiz');
		$data['region_id'] 			= $this->input->post('region_id');
		$data['periode'] 			= $this->input->post('start').'|'.$this->input->post('end');
		$data['image_cover'] 		= renameFile($_FILES['images']['name']);
		$data['template'] 			= $this->input->post('template');
		$data['num_of_winner'] 		= $this->input->post('num_of_winner');
		$data['status'] 			= $this->input->post('status');
		$data['title_en'] 			= $this->input->post('title_en');
		$data['title_ar'] 			= $this->input->post('title_ar');
		$data['title_fr'] 			= $this->input->post('title_fr');
		$data['title_ja'] 			= $this->input->post('title_ja');
		$data['title_ko'] 			= $this->input->post('title_ko');
		$data['title_zh-cn'] 		= $this->input->post('title_zh-cn');
		$data['title_zh-tw'] 		= $this->input->post('title_zh-tw');
		$data['description_en'] 	= $this->input->post('description_en');
		$data['description_ar'] 	= $this->input->post('description_ar');
		$data['description_fr'] 	= $this->input->post('description_fr');
		$data['description_ja'] 	= $this->input->post('description_ja');
		$data['description_ko'] 	= $this->input->post('description_ko');
		$data['description_zh-cn'] 	= $this->input->post('description_zh-cn');
		$data['description_zh-tw'] 	= $this->input->post('description_zh-tw');

        $tmp_images_dir = $this->config->item('tmp_images_dir');
        $images_dir = $this->config->item('images_dir');

        if (!empty($data['image_cover'])) {
			//~ echo '<pre>'; print_r($_FILES); #die(' qwerty');
            $destination = $images_dir.$data['image_cover'];
			$tmpImg = $_FILES['images']['tmp_name'];
            if (is_uploaded_file($tmpImg)) {
                move_uploaded_file($tmpImg, $destination);
            }

            // set for image thumbnail
			list($width, $height, $type, $attr) = getimagesize($destination);

			$config['image_library'] = 'gd2';
			$config['source_image'] = $destination;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			// we set to 50%
			//~ $res_width = 0.5 * $width;
			//~ $res_height = 0.5 * $height;
			$config['width'] = $width;
			$config['height'] = $height;
			$this->load->library('image_lib', $config);
			if ( !$this->image_lib->resize() ){
			    echo $this->image_lib->display_errors();
			}
        }

		$this->period_model->addNewItem($data);
		redirect('quizperiod');
	}

	public function edit($id)
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);

		$m_data["pages"] = '';
		$m_data["page"] = 'quiz';
		$m_data['dataPeriod'] = $this->period_model->getAllItems();
		$contents["menu"] = $this->load->view('vmenu',$m_data,true);

		$parsedData['regionData']	= $this->region_model->getAllItems();
		$parsedData['itemData'] = $this->period_model->getItemById($id);
		$contents["form"] = $this->load->view('form/vquizperiod_edit_form',$parsedData,true);
		$contents["vjs"] = $this->load->view('vjs',null,true);
		$this->load->view('vquizperiod_add',$contents);
	}

	function updatepost()
	{
		$data['id'] 				= $this->input->post('id');
		$data['region_quiz'] 		= $this->input->post('title_quiz');
		$data['region_id'] 			= $this->input->post('region_id');
		$data['periode'] 			= $this->input->post('start').'|'.$this->input->post('end');
		$data['template'] 			= $this->input->post('template');
		$data['num_of_winner'] 		= $this->input->post('num_of_winner');
		$data['status'] 			= $this->input->post('status');
		$data['title_en'] 			= $this->input->post('title_en');
		$data['title_ar'] 			= $this->input->post('title_ar');
		$data['title_fr'] 			= $this->input->post('title_fr');
		$data['title_ja'] 			= $this->input->post('title_ja');
		$data['title_ko'] 			= $this->input->post('title_ko');
		$data['title_zh-cn'] 		= $this->input->post('title_zh-cn');
		$data['title_zh-tw'] 		= $this->input->post('title_zh-tw');
		$data['description_en'] 	= $this->input->post('description_en');
		$data['description_ar'] 	= $this->input->post('description_ar');
		$data['description_fr'] 	= $this->input->post('description_fr');
		$data['description_ja'] 	= $this->input->post('description_ja');
		$data['description_ko'] 	= $this->input->post('description_ko');
		$data['description_zh-cn'] 	= $this->input->post('description_zh-cn');
		$data['description_zh-tw'] 	= $this->input->post('description_zh-tw');

        $tmp_images_dir = $this->config->item('tmp_images_dir');
        $images_dir = $this->config->item('images_dir');

        $rs['old_images'] = $this->input->post('old_images');

        if($_FILES['images']['name']!=''){
			$data['image_cover'] = renameFile($_FILES['images']['name']);

            // delete old image
            if (trim($rs['old_images']) != '' && file_exists($images_dir.$rs['old_images'])) {
                unlink($images_dir.$rs['old_images']);
            }

            $destination = $images_dir.$data['image_cover'];
			$tmpImg = $_FILES['images']['tmp_name'];

            if (is_uploaded_file($tmpImg)) {
                move_uploaded_file($tmpImg, $destination);
            }

            // set for image thumbnail
			list($width, $height, $type, $attr) = getimagesize($destination);

			$config['image_library'] = 'gd2';
			$config['source_image'] = $destination;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			// we set to 50%
			//~ $res_width = 0.5 * $width;
			//~ $res_height = 0.5 * $height;
			$config['width'] = $width;
			$config['height'] = $height;
			$this->load->library('image_lib', $config);
			if ( !$this->image_lib->resize() ){
			    echo $this->image_lib->display_errors();
			}
        }
        else{
			$data['image_cover'] = $rs['old_images'];
		}

        $this->period_model->updateItemById($data, $data['id']);
		redirect('quizperiod');
	}

	function delete($id)
	{
		$data = $this->period_model->getItemById($id);
        $images_dir = $this->config->item('images_dir');
		$is_delete = $this->period_model->deleteItemById($id);
		if (!empty($data['image_cover'])) {
			if ($is_delete) {
				unlink($images_dir.$data['image_cover']);
			} else {
				echo '<script>alert("Failed to removed");</script>';
			}
		}
		redirect('quizperiod');
	}

	function activated($id)
	{
		$data['status'] = 1;
		$is_delete = $this->period_model->activatedItemById($data,$id);
		if ($is_delete) {
			redirect('quizperiod');
		} else {
			echo '<script>alert("Failed to updated");</script>';
		}
	}

}
