<?php
class Quiz_model extends CI_Model {
    var $tableName = "questions";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function getLimitData($level, $offset, $limit)
	{
        $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("level", $level);
        $this->db->order_by("rand()"); 
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}

    function getItemById($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("id_question", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

}
