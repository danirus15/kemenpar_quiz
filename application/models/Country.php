<?php
class Country extends CI_Model {
    var $tableName = "country";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

    function getByRegionId($id)
    {
        $this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->order_by("country_name", "DESC");
        $this->db->where("region_id", $id);
        $query = $this->db->get();
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getAllItems()
    {
        $this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->where("region_id", "1");
        $this->db->order_by("country_name", "");
        $query = $this->db->get();
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

}
