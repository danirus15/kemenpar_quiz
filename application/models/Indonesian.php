<?php
class Indonesian extends CI_Model {
    var $tableName = "indonesian";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

    function getAllItems()
    {
        $this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getItemByGender($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("gender", $id);
        $query = $this->db->get();
        $array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getItemById($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("id", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

}
