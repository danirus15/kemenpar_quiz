<?php
class Users extends CI_Model {
    var $tableName = "users";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function insert_entry($data)
	{
		$input['nama']			= $data['nama'];
		$input['email'] 		= $data['email'];
		//~ $input['gender']		= $data['gender'];
		$input['register_from'] = $data['register_from'];
		$input['verified'] 		= $data['verified'];
		$input['user_id_fb']	= $data['user_id_fb'];
		$input['passwd']		= $data['passwd'];
		$input['token']			= $data['token'];

		$this->db->insert($this->tableName, $input);
		return $this->db->insert_id();
	}

    function getById($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("user_id", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

	public function countByIdFB($id)
	{
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('user_id_fb', $id);
		$query = $this->db->get();
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}

	public function getByIdFB($id)
	{
		$query = $this->db->get_where($this->tableName, array('user_id_fb' => $id));
		$array = $query->row_array();
		$query->free_result();
		return $array;
	}

    function getEmail($email)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("email", $email);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function getActivationEmail($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("token", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

	function addNewUser($data)
    {
        $this->db->insert($this->tableName, $data);
    }

    function updateData($data, $id)
    {
    	$this->db->update($this->tableName, $data, array('user_id' => $id));
    }

    function updatePassword($data, $email)
    {
    	return $this->db->update($this->tableName, $data, array('email' => $email));
    }

    function activationUser($data, $id)
    {
    	//$data['token'] 		= '';
    	$data['verified'] 	= '0';
    	$this->db->update($this->tableName, $data, array('token' => $id));
    }

	/* start login */
	public function cekLogin($email,$passwd)
	{
		$query = $this->db->get_where($this->tableName, array('email' => $email,'passwd' => $passwd,'verified' => 0));

		if ($query->num_rows() == 1){
			$res = $query->row_array();
			$query->free_result();

			$this->session->set_userdata('nama', $res['nama']);
			$this->session->set_userdata('user_id', $res['user_id']);
			$this->session->set_userdata('user_profile',$res);
			$this->session->set_userdata('login',true);

			return $res;
		}
		else{
			$this->session->set_flashdata('erroLogin', 'username dan / atau kata sandi tidak cocok, silahkan coba lagi atau hubungi administrator sistem Anda.');
			return FALSE;
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		return TRUE;
	}
	/* end login */

}
