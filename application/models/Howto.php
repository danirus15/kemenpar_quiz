<?php
class Howto extends CI_Model {
    var $tableName = "howto";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

    function getItemById($id)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("id", $id);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

}
