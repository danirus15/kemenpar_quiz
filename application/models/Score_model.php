<?php
class Score_model extends CI_Model {
    var $tableName = "scores";

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function getLimitData($offset, $limit)
	{
        $this->db->select("*");
		$this->db->from($this->tableName);
        $this->db->order_by("id_score", "DESC"); 
        $this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}

    function getItemById($key,$val)
    {
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where($key,$val);
        $query = $this->db->get();
        $array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
    }

    function countAllData()
    {
		return $this->db->count_all_results($this->tableName);
    }

    function addNewItem($data)
    {
        return $this->db->insert($this->tableName, $data);
    }

    function updateItemById($data,$key,$val)
    {
        return $this->db->update($this->tableName, $data, array($key => $val));
    }

    function deleteItemById($id)
    {
        $this->db->where('id_score', $id);
		$this->db->delete($this->tableName);
		return true;
    }

}
