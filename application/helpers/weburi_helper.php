<?php
function css_uri(){
    $CI =& get_instance();
    return $CI->config->item('css_uri');
}
function js_uri(){
    $CI =& get_instance();
    return $CI->config->item('js_uri');
}
function fonts_uri(){
    $CI =& get_instance();
    return $CI->config->item('fonts_uri');
}
function images_uri(){
    $CI =& get_instance();
    return $CI->config->item('images_uri');
}
function images_content(){
    $CI =& get_instance();
    return $CI->config->item('images_content');
}
?>
