<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 08/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Quiz extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('quiz_model');
		$this->load->model('score_model');
		$this->load->helper('generate_link');
		$this->load->library('session');
		if($this->session->userdata('login')=='') redirect('/');
		$this->userId = $this->session->userdata('user_id');
		//~ $this->userId = 1;
	}

	public function index($lang='')
	{
		$lang = $this->uri->segment(2,'en');

		$this->load->model('users');
		$datauser = $this->users->getById($this->userId);
		if($datauser['gender'] == '' or $datauser['country_id'] == '' or $datauser['region_id'] == ''){
			redirect('login/complete');
		}

		if($this->score_model->getItemById('user_id', $this->userId))
			redirect('quiz/finish/en');

		//~ unset($_SESSION['score_user_'.$this->userId]);
		$this->session->unset_userdata('score_user_'.$this->userId);

		$quiz  = [];

		$quiz_easy = $this->quiz_model->getLimitData(1, 0, 5);
		$quiz_hard = $this->quiz_model->getLimitData(2, 0, 5);

		foreach($quiz_easy as $easy) {
			$quiz[] = [
				'id'		=> (int)$easy['id_question'],
				'question' 	=> $easy['question_'.$lang],
				'answer'	=> [
					'a' => $easy['answer_a_'.$lang],
					'b' => $easy['answer_b_'.$lang],
					'c' => $easy['answer_c_'.$lang]
				],
				'hint' => generateLink($easy['hints_'.$lang]),
				'level' => 'Easy'
			];
		}

		foreach($quiz_hard as $easy) {
			$quiz[] = [
				'id'		=> (int)$easy['id_question'],
				'question' 	=> $easy['question_'.$lang],
				'answer'	=> [
					'a' => $easy['answer_a_'.$lang],
					'b' => $easy['answer_b_'.$lang],
					'c' => $easy['answer_c_'.$lang]
				],
				'hint' => generateLink($easy['hints_'.$lang]),
				'level' => 'Hard'
			];
		}

		$contents['lang'] = $lang;
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$contents["quiz"] = $quiz;
		$this->load->view('vquiz',$contents);
	}

	public function postAnswer($lang = '')
	{
		$lang = $this->uri->segment(3,'en');

		//~ if(!isset($_SESSION['score_user_'.$this->userId])) {
		if($this->session->userdata('score_user_'.$this->userId)=='') {
			$currentScore = [
				'true' => 0,
				'false' => 0,
				'score' => 0,
				'user_answer' => [],
				'last_time' => 0
			];

			//~ $_SESSION['score_user_'.$this->userId] = $currentScore;
			$this->session->set_userdata('score_user_'.$this->userId, $currentScore);
		}

		$currentScore = $this->session->userdata('score_user_'.$this->userId);

		$question = $this->quiz_model->getItemById($_POST['question_id']);

		$trueScore = $question['weight'];

		if(strtolower($_POST['answer']) === strtolower($question['corect_answer'])) {
			$currentScore['true']++;
			$currentScore['score'] = $currentScore['score'] + $trueScore;
			$status = true;
		} else {
			$currentScore['false']++;
			$status = false;
		}

		$currentScore['user_answer'][] = [
			'id' => $_POST['question_id'],
			'answer' => $_POST['answer'],
			'status' => $status,
			'score' => $status ? $trueScore : 0
		];

		$currentScore['last_time'] = $_POST['timer'];

		//~ $_SESSION['score_user_'.$this->userId] = $currentScore;
		$this->session->set_userdata('score_user_'.$this->userId, $currentScore);

		echo json_encode([
				'status' => 200,
				//~ 'data'  => $_SESSION['score_user_'.$this->userId]
				'data'  => $this->session->userdata('score_user_'.$this->userId)
		]);
	}

	public function postShare($media)
	{
		$currentScore = $this->score_model->getItemById('user_id', $this->userId);
		$scoreTotal = $currentScore['score_total'] + 200;

		if($media == 'fb') {
			$this->score_model->updateItemById([
				'score_total' 	=> $scoreTotal,
				'score_fb' 		=> 200
			], 'user_id', $this->userId);
		} elseif($media == 'tw') {
			$this->score_model->updateItemById([
				'score_total' 	=> $scoreTotal,
				'score_tw' 		=> 200
			], 'user_id', $this->userId);
		}

		echo json_encode([
			'status' => 200,
			'message' => 'success'
		]);
	}

	public function finish($lang = '')
	{
		//~ echo '<pre>'; var_dump($this->session->userdata('id_period')); die(' qwerty');
		$this->load->model('quizperiod');
		$this->load->model('users');

		$lang = $this->uri->segment(3,'en');

		$datauser = $this->users->getById($this->userId);

		if($this->score_model->getItemById('user_id', $this->userId)) {

		} else {
			//~ if(!isset($_SESSION['score_user_'.$this->userId]))
			if($this->session->userdata('score_user_'.$this->userId)=='')
				redirect('quiz');

			//~ $userScore = $_SESSION['score_user_'.$this->userId];
			$userScore = $this->session->userdata('score_user_'.$this->userId);

			$this->score_model->addNewItem([
				'user_id' => $this->userId,
				'score_quiz' => $userScore['score'],
				'correct_total' => $userScore['true'],
				'time'	=> $userScore['last_time'],
				'score_total' => $userScore['score'],
				'id_period' => $this->session->userdata('id_period'),
			]);

			//~ unset($_SESSION['score_user_'.$this->userId]);
			$this->session->unset_userdata('score_user_'.$this->userId);

			//sent email finish (say to congratulation)
			$this->load->library('email');

			$this->email->from('influencertrip.southeast@indonesia.travel', 'Wonderfull Indonesia');
			$this->email->to($datauser['email']);
			$this->email->subject('[Trips of Wonders] Congratulations');
			$this->email->message('
				<body style=margin:0;padding:0>
					<div style="margin:0;font-family:arial;font-size:13px;background:#fff;color:#555;border:1px solid #ccc">
						<div style=color:#b8b8b8;font-size:26px;text-align:center;background:#F0F0F0;padding:15px>
							<h1 style=font-size:22px;color:#000;margin:0;padding:0>Trip of Wonder 2016</h1>
						</div>
						<div style="margin:0 auto;text-align:center;background:#fff;padding:15px">
							<div style=color:#3b3b3b;font-size:18px;font-weight:700;margin-bottom:10px>Congratulations</div>
							<div style=line-height:130%;color:#6f6f6f>
								Congratulations! You finished the Indonesian Ministry of Tourism\'s Trip of Wonders Quiz. Want a better chance at winning the prize? Then get extra points by sharing this quiz on Facebook and Twitter. Login now to check your score http://www.artineu.co.id/quiz/login
							</div>
						</div>
						<div style=text-align:center;font-size:11px;padding:10px>
							Copyright © 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
						</div>
					</div>
				</body>
			');
			$this->email->set_mailtype("html");
			$this->email->send();
		}

		$contents['quizperiod'] = $this->quizperiod->getItemByRegionId($datauser['region_id']);
		$contents['score'] = $this->score_model->getItemById('user_id', $this->userId);
		$contents['lang'] = $lang;
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$this->load->view('vquiz_finish',$contents);
	}

}
