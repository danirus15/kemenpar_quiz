<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 27/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Home extends CI_Controller {

	public function index($lang='en')
	{
		$lang = $this->uri->segment(1,'en');

		$this->load->model('howto');
		$this->load->model('terms');
		$this->load->model('country');
		$this->load->model('quizperiod');
		$this->load->model('participant');
		$this->load->library('session');

		//~ $this->session->sess_destroy();

		$sess_region_id = $this->session->userdata('region_id');

		if($this->session->userdata('region_id')){
			$contents['howto'] = $this->howto->getItemById($sess_region_id);
			$contents['terms'] = $this->terms->getItemById($sess_region_id);
			$quizperiod = $this->quizperiod->getItemByRegionId($sess_region_id);
			$contents['quizperiod'] = $quizperiod;
			$contents['participant'] = $this->participant->getDataById($quizperiod['id']);
		}
		else {
			$contents['howto'] = $this->howto->getItemById(1);
			$contents['terms'] = $this->terms->getItemById(1);
			$quizperiod = $this->quizperiod->getItemByRegionId(1);
			$contents['quizperiod'] = $quizperiod;
			$contents['participant'] = $this->participant->getDataById($quizperiod['id']);
		}

		$contents['country'] = $this->country->getAllItems();

		$contents['lang'] = $lang;

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);

		$this->load->view('vhome', $contents);

	}

	public function welcome($lang='')
	{
		$this->load->model('howto');
		$this->load->model('terms');
		$this->load->model('country');
		$this->load->model('quizperiod');
		$this->load->model('region');
		$this->load->model('participant');
		$this->load->library('session');

		if($this->input->post('country_select')){
			$split=explode('-',$this->input->post('country_select'));
			$this->session->set_userdata('region_id', $split[1]); //create session region_id
			$sess_region_id = $this->session->userdata('region_id');
			if($this->session->userdata('region_id')!=""){
				$region = $this->region->getItemById($sess_region_id);
				$lang = $region['default_lang'];

				$contents['howto'] = $this->howto->getItemById($sess_region_id);
				$contents['terms'] = $this->terms->getItemById($sess_region_id);
				$quizperiod = $this->quizperiod->getItemByRegionId($sess_region_id);
				$this->session->set_userdata('id_period', $quizperiod['id']); //create session id_period
				$contents['quizperiod'] = $quizperiod;
				$contents['participant'] = $this->participant->getDataById($quizperiod['id']);
			}
			else {
				$lang = 'en';
				$contents['howto'] = $this->howto->getItemById(1);
				$contents['terms'] = $this->terms->getItemById(1);
				$quizperiod = $this->quizperiod->getItemByRegionId(1);
				$contents['quizperiod'] = $quizperiod;
				$contents['participant'] = $this->participant->getDataById($quizperiod['id']);
			}
		}
		else {
			$lang = $this->uri->segment(2,'en');
			$contents['howto'] = $this->howto->getItemById(1);
			$contents['terms'] = $this->terms->getItemById(1);
			$quizperiod = $this->quizperiod->getItemByRegionId(1);
			$contents['quizperiod'] = $quizperiod;
			$contents['participant'] = $this->participant->getDataById($quizperiod['id']);
		}

		//~ echo '<pre>'; var_dump($this->session->userdata('id_period')); die(' qwerty');
		$contents['country'] = $this->country->getAllItems();

		$contents['lang'] = $lang;

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["header"] = $this->load->view('vheader',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);

		$this->load->view('vhome', $contents);
	}

}
