<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 08/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Forgotpassword extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('users');
	}

	public function index()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);

		if($_POST){
			$email = strtolower($this->input->post('email'));
			$cek = $this->users->getEmail($email);
			if(count($cek)>0){
				$newpass = strtolower(md5(uniqid(rand(), true)));
				$newpass = substr($newpass, 0, 20);
				$newdata['passwd'] = md5($newpass);
				$this->users->updatePassword($newdata, $email);

				$this->load->library('email');

				$this->email->from('influencertrip.southeast@indonesia.travel', 'Wonderfull Indonesia');
				$this->email->to($cek['email']);
				$this->email->subject('[Trips of Wonders] New Password');
				$this->email->message('
					<body style=margin:0;padding:0>
						<div style="margin:0;font-family:arial;font-size:13px;background:#fff;color:#555;border:1px solid #ccc">
							<div style=color:#b8b8b8;font-size:26px;text-align:center;background:#F0F0F0;padding:15px>
								<h1 style=font-size:22px;color:#000;margin:0;padding:0>Trip of Wonder 2016</h1>
							</div>
							<div style="margin:0 auto;text-align:center;background:#fff;padding:15px">
								<div style=color:#3b3b3b;font-size:18px;font-weight:700;margin-bottom:10px>Your email : '.$cek['email'].'</div>
								<div style=color:#3b3b3b;font-size:18px;font-weight:700;margin-bottom:10px>Your New Password : '.$newpass.'</div>
								<div style=line-height:130%;color:#6f6f6f>Login Now<br>'.base_url().'login</div>
							</div>
							<div style=text-align:center;font-size:11px;padding:10px>
								Copyright © 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
							</div>
						</div>
					</body>
				');
				$this->email->set_mailtype("html");
				$this->email->send();

				$contents["msg"] = "Cek your email";
				$this->load->view('vpassword',$contents);
			}
			else{
				$contents["msg"] = "Email not registered";
				$this->load->view('vpassword',$contents);
			}
		}
		else{
			$this->load->view('vpassword',$contents);
		}
	}

}
