<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 11/07/2016
 * @version $Id$
 * @copyright 2016
*/

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('facebook');
		$this->load->model('users');
		if($this->session->userdata('login')!=''){
			redirect('login/profile');
		}
	}

	public function index()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);

		$contents["vheader2"] = $this->load->view('vheader2',null,true);

		$contents['loginfb'] = $this->facebook->login_url();
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$this->load->view('vregister',$contents);
	}

	public function go()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);

		$contents["vheader2"] = $this->load->view('vheader2',null,true);

		$contents['loginfb'] = $this->facebook->login_url();
		$contents["alljs"] = $this->load->view('vjs',null,true);

		$data['nama'] 			= $this->input->post('name');
		$data['email'] 			= $this->input->post('email');
		$data['register_from'] 	= 'email';
		$data['verified'] 		= 1;
		$data['passwd'] 		= md5($this->input->post('passwd'));
		$data['token'] 			= md5($this->input->post('email').$this->input->post('passwd'));

		$cek = $this->users->getEmail($data['email']);

		if(count($cek)>0){
			//echo 'udah ada';
			$contents["msg"] = "Email already registered";
			$this->load->view('vregister',$contents);
			//redirect('../register');
		}
		elseif(strlen($this->input->post('passwd'))<5){
			$contents["msg"] = "Password too short. Minimum Password 5 character";
			$this->load->view('vregister',$contents);
		}
		else{
			$this->users->addNewUser($data);
			$this->load->library('email');

			$this->email->from('influencertrip.southeast@indonesia.travel', 'Wonderfull Indonesia');
			$this->email->to($data['email']);
			$this->email->subject('[Trips of Wonders] Verify your email');
			$this->email->message('
				<body style=margin:0;padding:0>
					<div style="margin:0;font-family:arial;font-size:13px;background:#fff;color:#555;border:1px solid #ccc">
						<div style=color:#b8b8b8;font-size:26px;text-align:center;background:#F0F0F0;padding:15px>
							<h1 style=font-size:22px;color:#000;margin:0;padding:0>Trip of Wonder 2016</h1>
						</div>
						<div style="margin:0 auto;text-align:center;background:#fff;padding:15px">
							<div style=color:#3b3b3b;font-size:18px;font-weight:700;margin-bottom:10px>Verify your email</div>
							<div style=line-height:130%;color:#6f6f6f>
								Click to activation your account<br>'.base_url().'register/verify/?token='.$data['token'].'
							</div>
						</div>
						<div style=text-align:center;font-size:11px;padding:10px>
							Copyright © 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
						</div>
					</div>
				</body>
			');
			$this->email->set_mailtype("html");

			$this->email->send();
			redirect('register/success');
		}

	}

	public function success()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$this->load->view('vregister_success',$contents);
	}

	public function verify()
	{
		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents["alljs"] = $this->load->view('vjs',null,true);

		$data['token'] = $this->input->get('token');

		$cek = $this->users->getActivationEmail($data['token']);

		if(count($cek)>0){
			$this->load->library('email');

			$this->email->from('influencertrip.southeast@indonesia.travel', 'Wonderfull Indonesia');
			$this->email->to($cek['email']);
			$this->email->subject('[Trips of Wonders] Activation Account Successfull');
			$this->email->message('
				<body style=margin:0;padding:0>
					<div style="margin:0;font-family:arial;font-size:13px;background:#fff;color:#555;border:1px solid #ccc">
						<div style=color:#b8b8b8;font-size:26px;text-align:center;background:#F0F0F0;padding:15px>
							<h1 style=font-size:22px;color:#000;margin:0;padding:0>Trip of Wonder 2016</h1>
						</div>
						<div style="margin:0 auto;text-align:center;background:#fff;padding:15px">
							<div style=color:#3b3b3b;font-size:18px;font-weight:700;margin-bottom:10px>congratulation</div>
							<div style=line-height:130%;color:#6f6f6f>Login Now<br>'.base_url().'login</div>
						</div>
						<div style=text-align:center;font-size:11px;padding:10px>
							Copyright © 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
						</div>
					</div>
				</body>
			');

			$this->email->send();

			$this->users->activationUser($data, $data['token']);
			$this->load->view('vregister_activation',$contents);
		}
		else{
			$this->load->view('vregister_activation_fail',$contents);
		}
	}

}
