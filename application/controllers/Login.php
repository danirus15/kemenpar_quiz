<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Willy Kristianto
 *
 * Email: willykristianto89@gmail.com
 *
 * Create Date: 27/06/2016
 * @version $Id$
 * @copyright 2016
*/

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('facebook');
		$this->load->library('session');
		$this->load->model('users');
	}

	public function index()
	{
		if($this->session->userdata('login') === true){
			redirect('login/profile');
		}

		if ($this->facebook->logged_in()) {
			$user = $this->facebook->user();
			//~ echo '<pre>'; print_r($user); die(' qwerty');
			$picture_profile = $this->fetch('https://graph.facebook.com/v2.5/'.$user['data']['id'].'/picture?type=large');
			$user['data']['picture'] = $picture_profile['url'];
			if ($user['code'] === 200) {
				$this->session->set_userdata('login',true);
				$this->session->set_userdata('nama',$user['data']['name']);
				$this->session->set_userdata('user_id',$user['data']['id']);
				$postdata['nama'] = $user['data']['name'];
				$postdata['email'] = $user['data']['email'];
				//~ $postdata['gender'] = $user['data']['gender'];
				$postdata['register_from'] = 'fb';
				$postdata['verified'] = 0;
				$postdata['user_id_fb'] = $user['data']['id'];
				$postdata['passwd'] = md5($user['data']['name']);
				$postdata['token'] = md5($user['data']['name'].$user['data']['email']);

				if($this->users->countByIdFB($user['data']['id']) > 0) { //cek apakah sudah terdaftar?
					$this->session->set_userdata('nama',$user['data']['name']);
					redirect('login/profile');
				}
				else{
					$last_id = $this->users->insert_entry($postdata);
					$this->session->set_userdata('user_id',$last_id);
					redirect('login/profile');
				}
			}
		}
		else {
			$contents["head"] = $this->load->view('vhead',null,true);
			$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
			$contents['loginfb'] = $this->facebook->login_url();
			$contents["alljs"] = $this->load->view('vjs',null,true);
			$contents["vheader2"] = $this->load->view('vheader2',null,true);
			$this->load->view('vlogin',$contents);
		}
	}

	public function dologin()
	{
		$email = $this->input->post('email');
		$passwd = md5($this->input->post('passwd'));

		$cek = $this->users->cekLogin($email,$passwd);
		if($cek){
			if($cek['country_id']=='' or $cek['region_id']=='' or $cek['gender']==''){
				redirect('login/complete');
			}
			else{
				redirect('login/profile');
			}
		}
		else{
			$contents["msg"] = "Email or password incorrect";
			$contents["head"] = $this->load->view('vhead',null,true);
			$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
			$contents['loginfb'] = $this->facebook->login_url();
			$contents["alljs"] = $this->load->view('vjs',null,true);
			$contents["vheader2"] = $this->load->view('vheader2',null,true);
			$this->load->view('vlogin',$contents);
		}
	}

	public function profile()
	{
		if($this->session->userdata('login') !== true){
			redirect('/');
		}

		$user_id = $this->session->userdata('user_id');
		//~ echo '<pre>'; print_r($session_user); die(' qwerty');
		$data = $this->users->getById($user_id);

		if($data['gender'] == '' or $data['country_id'] == '' or $data['region_id'] == ''){
			redirect('login/complete');
		}

		$this->load->model('indonesian');

		$picture_profile = $this->fetch('https://graph.facebook.com/v2.5/'.$user_id.'/picture?type=large');
		if($picture_profile['url']!=''){
			$contents['picture'] = $picture_profile['url'];
		}
		else{
			$contents['picture'] = '';
		}

		$gender = $this->indonesian->getItemByGender(strtolower($data['gender']));
		foreach($gender as $val_gender){
			$arr_gender[] = $val_gender['nama'];
		}
		$v = array_rand($arr_gender,1);


		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents['user_profile'] = $data;
		$contents['name_gender'] = $arr_gender[$v];
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$this->load->view('vprofile',$contents);
	}

	public function complete()
	{
		if($this->session->userdata('login') !== true){
			redirect('/');
		}

		$this->load->model('country');

		$user_id = $this->session->userdata('user_id');
		$data = $this->users->getById($user_id);
		//~ echo '<pre>'; print_r($data); die(' qwerty');

		$picture_profile = $this->fetch('https://graph.facebook.com/v2.5/'.$user_id.'/picture?type=large');
		$contents['picture'] = $picture_profile['url'];

		$contents['country'] = $this->country->getAllItems();

		$contents["head"] = $this->load->view('vhead',null,true);
		$contents["boxlanguage"] = $this->load->view('vboxlanguage',null,true);
		$contents["vheader2"] = $this->load->view('vheader2',null,true);
		$contents['user_profile'] = $data;
		$contents["alljs"] = $this->load->view('vjs',null,true);
		$this->load->view('vcomplete',$contents);
	}

	public function docomplete()
	{
		if(!$_POST){
			redirect('/');
		}
		else{
			list($country,$region) = explode('-',$this->input->post('country'));
			$data['gender'] = $this->input->post('gender');
			$data['country_id'] = $country;
			$data['region_id'] = $region;
			$this->users->updateData($data,$this->input->post('id'));

			redirect('login/profile');
		}
	}

	private function fetch($url, $cookiesIn = '')
	{
		$options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE         => $cookiesIn
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m"; 
        preg_match_all($pattern, $header_content, $matches); 
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
		return $header;
	}
}
