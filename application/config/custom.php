<?php
// path and directory
$config['doc_root'] 		= 'http://'.$_SERVER["HTTP_HOST"].'/quiz/';
$config['table_total_rows']	= 10;
$config['template_uri']		= $config['doc_root'] .'public/';
$config['css_uri']			= $config['template_uri'] .'css/';
$config['js_uri']			= $config['template_uri'] .'js/';
$config['fonts_uri']		= $config['template_uri'] .'fonts/';
$config['images_uri']		= $config['template_uri'] .'images/';

$config['images_content']	= $config['doc_root'] .'xdata/images/';
?>
