<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Wonderfull Indonesia</title>

	<link rel="shortcut icon" href="<?=images_uri();?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?=images_uri();?>faficon.gif" type="image/x-icon">

	<link href="<?=css_uri();?>frame.style.css" rel="stylesheet" type="text/css" />
	<link href="<?=css_uri();?>fonts.css" rel="stylesheet" type="text/css" />
	<link href="<?=css_uri();?>style.css" rel="stylesheet" type="text/css" />
	<link href="<?=css_uri();?>style.responsive.css" rel="stylesheet" type="text/css" />
</head>
