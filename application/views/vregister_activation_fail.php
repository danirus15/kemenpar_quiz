<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="title">Verify Account</div>
		<div class="desc">
		Your account unregistered
		</div>
	    <div class="cleafix pt30"></div>
	    <b><a href="<?=base_url();?>register" class="l_blue">Register Now</a></b>
	</div>

	<?=$alljs;?>
</body>
</html>
