	<div id="header">
		<div class="container">
			<a href="<?=base_url();?>" class="logo"><img src="<?=images_uri();?>logo.png" alt=""></a>
			<div class="menu-mobile menuclick">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="menu-mobile menu-on">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
		<div class="nav">
			<a href="#home">Home</a>
			<a href="#how">How to Play</a>
			<a href="#prize">Prize</a>
			<a href="#terms">Terms & Conditions</a>
		</div>
		<div class="clearfix"></div>
		<div class="login-area">
			<?php
			if($this->session->userdata('region_id')) {
				//echo'Hello Andi | <a href="#">Sign Out</a>';
			}
			else{}
			?>
		</div>
		<div class="language-box">
			<div class="selectbox">Language - English</div>
			<div class="box-select">
				<div class="title"><b>Select your language</b></div>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/ar';?>">العربية</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/en';?>">English</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/fr';?>">Français</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/ja';?>">日本語</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/ko';?>">한국어</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/zh-cn';?>">简体中文</a>
				<a href="http://<?=$_SERVER['HTTP_HOST'].'/zh-tw';?>">繁體中文</a>
			</div>
		</div>
	</div>
