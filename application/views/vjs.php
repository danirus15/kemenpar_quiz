<div id="footer">
	<div class="container">
			Copyright &copy; 2016 Ministry of Tourism, Republic of Indonesia. All rights reserved
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript" src="<?=js_uri();?>jquery.js"></script>
<script type="text/javascript" src="<?=js_uri();?>sticky-scroll.js"></script>
<script type="text/javascript" src="<?=js_uri();?>jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?=js_uri();?>jquery-imagefill.js"></script>
<script type="text/javascript" src="<?=js_uri();?>jquery.fallings.js"></script>
<?php if($this->uri->segment(1)=='quiz'): ?>
<script type="text/javascript" src="<?=js_uri();?>timer.jquery.min.js"></script>
<?php endif; ?>
<script type="text/javascript" src="<?=js_uri();?>controller.wp.js"></script>
