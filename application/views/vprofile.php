<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="profil">
			<div class="img-profil fill-img">
				<img src="<?=$picture;?>" alt="<?=$user_profile['nama'];?>">
			</div>
		</div>
		<br>
		<div class="title">Hello <?=$user_profile['nama'].' '.$name_gender;?></div>
		You’re almost there! Just answer these 10 ten questions and you’ll be in the running for an awesome trip to paradise on earth
		<form action="<?=base_url();?>quiz" method="post" class="form">
			<input type="submit" value="Start Your Journey Now!" class="btn_start">
		</form>
	</div>

	<?=$alljs;?>
</body>
</html>
