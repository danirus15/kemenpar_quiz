<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$header;?>

	<div class="cover" id="intro">
		<div class="img main-title">
			<img src="<?=images_content().$quizperiod['image_cover'];?>" alt="<?=$quizperiod['title_'.$lang];?>">
		</div>
		<div class="overlay"></div>
		<div class="text">
			<h1><?=$quizperiod['title_'.$lang];?></h1>
			<div class="desc">
				<?=$quizperiod['description_'.$lang];?>
				<br>
				<a href="#how" class="btn-start">START YOUR JOURNEY!</a>
			</div>
		</div>
	</div>

	<div id="how" class="section">
		<div class="container animation-element bounce-up">
			<h2 class="a1"><?=$howto['title_'.$lang];?></h2>
			<div class="desc a1 a2">
				<p><?=$howto['description_'.$lang];?></p>
				<div class="howto">
					<div class="h1 a1 a3">
						<span class="no">1</span>
						<img src="<?=images_uri();?>01.png" alt="">
						<div class="clearfix"></div>
						<div class="title"><?=$howto['step1_'.$lang];?></div>
						<div class="clearfix"></div>
						
					</div>
					<div class="h1 a1 a4">
						<span class="no">2</span>
						<img src="<?=images_uri();?>02.png" alt="">
						<div class="clearfix"></div>
						<div class="title"><?=$howto['step2_'.$lang];?></div>
						<div class="clearfix"></div>
					</div>
					<div class="h1 a1 a5">
						<span class="no">3</span>
						<img src="<?=images_uri();?>03.png" alt="">
						<div class="clearfix"></div>
						<div class="title"><?=$howto['step3_'.$lang];?></div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<a href="<?=base_url();?>register" class="btn_start a1 a6">Start Your Journey Now!</a>
			</div>
		</div>
		<div class="clearfix batas"></div>
	</div>

	<div class="section s_prize" id="prize">
		<div class="container prize animation-element bounce-up">
			<h2 class="a1">Prize</h2>
			<div class="prize">
				<img src="<?=images_uri();?>04.png" alt="" class="a1 a2">
				<div class="clearfix"></div>
				<div class="text a1 a3">
					<div class="title">
						<b><?=$howto['title_prize_'.$lang];?></b>
						<?=$howto['desc_prize_'.$lang];?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<br>
		<div class="img_gal animation-element bounce-up">
			<?php
				if($theme=="Sensory-Wonders"){
					$img = array(
					"1" => "b-01.jpg", 
					"2" => "b-02.jpg",
					"3" => "b-03.jpg",
					"4" => "b-04.jpg",
					"5" => "b-05.jpg");
				}
				elseif($theme=="Adventurous-Wonders"){
					$img = array(
					"1" => "b-01.jpg", 
					"2" => "b-02.jpg",
					"3" => "b-03.jpg",
					"4" => "b-04.jpg",
					"5" => "b-05.jpg");
				}
				else{
					$img = array(
					"1" => "a-01.jpg", 
					"2" => "a-02.jpg",
					"3" => "a-03.jpg",
					"4" => "a-04.jpg",
					"5" => "a-05.jpg",
					"6" => "a-06.jpg",
					"7" => "a-07.jpg");
				}
			?>
			<?php foreach ($img as $i => $img2): ?>
				<div class="ratio4_3 box_img a1 a<?=$i;?>">
					<div class="img_con"><img src="<?=base_url().'public/gal/'.$img2;?>"/></div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="section animation-element bounce-up" id="participant">
		<div class="container2">
			<h2 class="a1">Top Participant</h2>
			<div class="top-pos a1 a2">
			<?php
			if(count($participant) >= $quizperiod['num_of_winner']){
				$total = $quizperiod['num_of_winner'];
			}
			else{
				$total = count($participant);
			}

			if($total>0):
			?>
				<ul>
				<?php for($i=0; $i<$total; $i++): ?>
					<li>
						<div class="no"><?=$i+1;?></div>
						<div class="name"><?=$participant[$i]['nama'];?></div>
						<div class="clearfix"></div>
					</li>
				<?php endfor; ?>
				</ul>
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="section s_term s_komodo animation-element bounce-up" id="terms">
		<div class="container">
			<h2 class="a1"><?=$terms['title_'.$lang];?></h2>
			<div class="detail_text a1 a2">
				<?=$terms['description_'.$lang];?>
			</div>
		</div>
	</div>

	<?php
	if($this->session->userdata('region_id')){}
	else {
	?>
	<div class="pop_nationality">
		<div class="container_pop">
			<div class="head">
				<img src="<?=images_uri();?>logo.png" alt="">
			</div>
			<strong>Select your nationality</strong>
		
				<form action="<?=base_url();?>welcome" method="post" class="form_nationality">
				<div class="styled-select">
					<select name="country_select" required>
						<option value="">Select Country</option>
					<?php foreach($country as $row): ?>
						<option value="<?=$row["country_id"];?>-<?=$row["region_id"];?>"><?=$row['country_name'];?></option>
					<?php endforeach; ?>
					</select>
				</div>
				<div class="clearfix pt20"></div>
				<input type="submit" value="Submit" class="btn">
			</form>
		</div>
	</div>
	<?php
	}
	?>

	<?=$alljs;?>
</body>
</html>
