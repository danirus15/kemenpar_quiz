<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="title">Register</div>
		<div class="desc">
		Check your email to verify your account
		</div>
	    <div class="cleafix pt30"></div>
	    <br><br>
	    <b><a href="<?=base_url();?>login" class="l_blue">Login Now</a></b>
	</div>

	<?=$alljs;?>
</body>
</html>
