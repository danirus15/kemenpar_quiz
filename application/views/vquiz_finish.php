<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<div class="quiz_finish">
		<div class="img-cover fill-img">
			<img src="<?=images_content().$quizperiod['image_cover'];?>" alt="<?=$quizperiod['title_'.$lang];?>">
		</div>
		<div class="overlay"></div>
		<div class="container">
			<div class="logo"><img src="<?=images_uri();?>logo.png" alt=""></div><br><br>
			<div class="desc">
				<b>Great! </b><br>
				You're finally done!
			</div>
			<br>
			<div class="clearfix pt20"></div>
			YOUR SCORE<br>
			<div class="score" id="score">
				<?=$score['score_total']?>
			</div>
			<div class="clearfix pt20"></div>
			<?=$score['correct_total']?> of 10 Correct Answer<br>
			Time: <?=substr($score['time'],0,5)?>
			<br><br><br>
			Want to collect more points?<br>
			Just share this quiz with your friends by clicking below! <br>
			The more you share, the more you gain.<br>

			<div class="sosmed">
				<?php if(!$score['score_fb']): ?>
				<a href="#" id="fb_share">
					<img src="<?=images_uri();?>ico_fb.png" alt="">
					<span>Share to Facebook</span>
				</a>
				<?php endif; ?>
				<?php if(!$score['score_tw']): ?>
				<a href="https://twitter.com/intent/tweet?url=http://www.artineu.co.id/quiz/&text=Open this to win a trip to Indonesia! #WonderfulIndonesia #tripofwonders" class="tw" id="tw_share">
					<img src="<?=images_uri();?>ico_tw.png" alt="">
					<span>Share to Twitter</span>
				</a>
				<?php endif; ?>
			</div>
			<br><br>
			<a href="<?=base_url();?>" class="link1">Back to Home</a>
			<a href="<?=base_url();?>logout" class="link1">Logout</a>
		</div>
	</div>

	<?=$alljs;?>

    <script>
        var base_url = '<?=base_url();?>quiz/'
        var current_score = parseInt($('#score').html())
        $('#fb_share').click(function(e) {
            e.preventDefault()
            $.ajaxSetup({ cache: true });
                $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
                FB.init({
                    appId: '223450651160184',
                    version: 'v2.3'
                })
                FB.ui({
                    method: 'share',
                    title: 'tripofwonders',
                    description: 'Try this for a chance to win a trip to Indonesia! #WonderfulIndonesia #tripofwonders',
                    href: 'http://www.artineu.co.id/quiz/',
                  },
                  function(response) {
                      if (response && !response.error_code) {
                          console.log('Posting completed.')
                          $.getJSON(base_url + 'postShare/fb?post_id=' + response.post_id, function() {
                              $('#fb_share').fadeOut()
                              current_score = current_score + 200
                              $('#score').html(current_score)
                          })
                      } else {
                          console.log('Posting canceled.')
                      }
                  })
              })
        })

        window.twttr = (function (d,s,id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
        js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
        }(document, "script", "twitter-wjs"));

        //Once twttr is ready, bind a callback function to the tweet event
        twttr.ready(function(twttr) {
            twttr.events.bind('tweet', function (event) {
                console.log('Tweet completed');
                $.getJSON(base_url + 'postShare/tw', function() {
                    $('#tw_share').fadeOut()
                    current_score = current_score + 200
                    $('#score').html(current_score)
                })
            });
        });
    </script>
</body>
</html>
