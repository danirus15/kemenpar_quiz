<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="title">Register</div>
		<div class="desc">
		Please fill out below to start the quiz
		</div>
		<form action="<?=base_url('register');?>/go"  method="post" class="form">
		<?php 
			if(isset($msg)){
				echo'<div class="notif">';
				echo $msg;
				echo'</div>';
			}
		?>
	        <strong>Name</strong>
	        <input type="text" name="name" class="input" placeholder="Your Name" required>
	        <strong>Email</strong>
	        <input type="text" name="email" class="input"  placeholder="Your Email" required>
	        <strong>Password</strong>
	        <input type="password" name="passwd" class="input"  placeholder="Password" required>
	        <div class="clearfix"></div>
	        <input type="submit" value="Register" class="btn_start">
	    </form>
	    <br>
	    or register with facebook
	    <div class="cleafix pt10"></div>
		<div class="sosmed">
			<a href="<?=$loginfb;?>" class="fb2">
				<img src="<?=images_uri();?>ico_fb.png" alt="">
				<span>Connect with Facebook</span>
			</a>
		</div>
	    <div class="cleafix pt10"></div>
	    <b>Already register? <a href="<?=base_url();?>login" class="l_blue">Login Now</a></b>
	</div>

	<?=$alljs;?>
</body>
</html>
