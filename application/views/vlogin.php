<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="title">Login</div>
		<div class="desc"></div>
		<form action="<?=base_url();?>login/dologin"  method="post" class="form">
		<?php 
			if(isset($msg)){
				echo'<div class="notif">';
				echo $msg;
				echo'</div>';
			}
		?>
			<strong>Email</strong>
			<input type="text" name="email" class="input" placeholder="Your Email">
			<strong>Password</strong>
			<input type="password" name="passwd" class="input" placeholder="Password">
			<div class="clearfix"></div>
			<input type="submit" value="Login" class="btn_start">
		</form>
		<br>
		or login with facebook
		<div class="cleafix pt10"></div>
		<div class="sosmed">
			<a href="<?=$loginfb;?>" class="fb2">
				<img src="<?=images_uri();?>ico_fb.png" alt="">
				<span>Connect with Facebook</span>
			</a>
		</div>
		<div class="cleafix pt10"></div>
		<b><a href="<?=base_url();?>forgotpassword" class="l_blue">Forget Password</a></b><br><br>
		<b><a href="<?=base_url();?>register" class="l_blue">Register</a></b>
	</div>

	<?=$alljs;?>
</body>
</html>
