<?=$head;?>
<script id="question-template" type="text/x-handlebars-template">
	<input type="hidden" name="question_id" value="{{ id }}" >
	<div class="q" id="question">{{ question }}</div>
	<div class="a radiocheck">
		<label>
			<input type="radio" name="answer" value="a">
			<div class="check"></div>
			<div class="a1" id="answer_a">
				{{ answer.a }}
			</div>
			<div class="clearfix"></div>
		</label>
		<label>
			<input type="radio" name="answer" value="b">
			<div class="check"></div>
			<div class="a1" id="answer_b">
				{{ answer.b }}
			</div>
			<div class="clearfix"></div>
		</label>
		<label>
			<input type="radio" name="answer" value="c">
			<div class="check"></div>
			<div class="a1" id="answer_c">
				{{ answer.c }}
			</div>
			<div class="clearfix"></div>
		</label>
	</div>
	<div class="hints">
		Check qlue on {{{ hint }}}
	</div>
</script>

<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$vheader2;?>

	<div class="container quiz_con quiz_con2">
		<div id="quiz_pg">
			<form action="" method="post" id="quiz_form">
				<div id="question_container"></div>
				<div class="clearfix pt20"></div>
				<div class="time_count">
					<span>TIME</span>
					<input type='text' name='timer' class='form-control timer' placeholder='00.00' />
				</div>
				<div align="center">
					<input type="submit" value="Next Question" class="btn_start" id="btn_submit">
				</div>
			</form>
		</div>
	</div>

	<?=$alljs;?>
	<script type="text/javascript" src="<?=js_uri();?>handlebars-v4.0.5.js"></script>
	<script>
		var base_url = '<?=base_url();?>'
		var lang = '<?=$lang?>'
		var question_template = Handlebars.compile($('#question-template').html())
		var question_number = 0

		var questions = <?=json_encode($quiz)?>

		var hints_timer = setTimeout(function() {
			$(".hints").slideDown();
		}, 10000)

		var radiocheck = {
			init: () => {
				$('.radiocheck label').click(function(){
			    	$(".radiocheck label").removeClass("selected")
					$(this).addClass("selected")
					$('#btn_submit').prop('disabled', false)
				})
				$('.radiocheck label.kosong').click(function(){
					$(".radiocheck label").removeClass("selected")
			    	$(this).addClass("selected")
					$('#btn_submit').prop('disabled', false)
				})
			}
		}

		var show_hint = () => {
			clearTimeout(hints_timer)
			$(".hints").hide()
			hints_timer = setTimeout(function() {
			  $(".hints").slideDown();
			}, 10000)
		}

		$(document).ready(function() {
			$('#btn_submit').prop('disabled', true)
			$('#question_container').html(question_template(questions[question_number]))
			radiocheck.init()
			show_hint()

			$('#quiz_form').submit(function(event) {
	            var formData = new FormData($('#quiz_form')[0])
	            $('#btn_submit').prop('disabled', true).val('Loading...')
				$('#question_container').html('Loading next question...')
	            $.ajax({
	                type: "POST",
	                url: base_url + 'quiz/postAnswer/' + lang,
	                data: formData,
	                cache: false,
	                contentType: false,
	                processData: false,
	                dataType: "json",
	                success: function(data)
	                {
						console.log(data)
	                    question_number++
						if(question_number <= 9) {
							$('#question_container').html(question_template(questions[question_number]))
							radiocheck.init()
							show_hint()
							$('#btn_submit').prop('disabled', true).val('Next Question')
							$('#question_container').fadeIn()
						} else {
							window.location = base_url + 'quiz/finish/' + lang;
						}

	                },
	                error: function (xhr, ajaxOptions, thrownError) {
	                    error = '<li>Internal Server Error: '+xhr.status+'. Try again.</li>'
	                    $('.saveButton').removeAttr('disabled').html('Save')
	                    $('#error_box').show()
	                    $('#error_list').empty().html(error)
	                    $("html, body").animate({ scrollTop: 0 }, "slow")
	                },
	            })
	            event.preventDefault()
	        })
		})
	</script>
</body>
</html>
