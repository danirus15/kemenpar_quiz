<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="profil">
			<div class="img-profil fill-img">
				<img src="<?=$picture;?>" alt="<?=$user_profile['nama'];?>">
			</div>
		</div>
		<br>
		<div class="title">Hello <?=$user_profile['nama'];?></div>
		<div class="desc">Please complete your registation</div>

		<form action="<?=base_url();?>login/docomplete" method="post" class="form" onsubmit="return validateForm()" name="confirm">
			<div class="notif">Select your gender!</div>
			<strong>Name</strong>
			<div class="info"><?=$user_profile['nama'];?></div>
			<strong>Email Address</strong>
			<div class="info"><?=$user_profile['email'];?></div>
			<strong>Gender</strong>
			<div class="infogender radiocheck">
				<label class="gender">
					<div class="check"></div>
					<input type="radio" name="gender" value="male" required>
					Male
				</label>
				<label class="gender">
					<div class="check"></div>
					<input type="radio" name="gender" value="female" required>
					Female
				</label>
			</div>
			<div class="clearfix"></div>
			<strong>Country Passport</strong>
			<select name="country" id="" class="input_select" required>
				<option value="">Select Country</option>
				<?php foreach($country as $row): ?>
					<option value="<?=$row["country_id"];?>-<?=$row["region_id"];?>"><?=$row['country_name'];?></option>
				<?php endforeach; ?>
			</select>
			<div class="clearfix pt20"></div>
			<input type="hidden" name="id" value="<?=$user_profile['user_id'];?>">
			<input type="submit" value="Continue" class="btn_start">
		</form>
	</div>
	<script type="text/javascript">
	function validateRadio (radios)
	{
		for (i = 0; i < radios.length; ++ i)
		{
			if (radios [i].checked) return true;
		}
		return false;
	}

	function validateForm()
	{
		if(validateRadio (document.forms["confirm"]["gender"]))
		{
			return true;
		}
		else
		{
			alert('Please answer all questions');
			return false;
		}
	}
	</script>

	<?=$alljs;?>
</body>
</html>
