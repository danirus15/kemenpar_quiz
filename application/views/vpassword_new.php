<!DOCTYPE html>
<html>
<?=$head;?>
<?php
if($this->session->userdata('region_id')=="1"){
	$theme="Sensory-Wonders";
}
elseif($this->session->userdata('region_id')=="2"){
	$theme="Adventurous-Wonders";
}
else {
	$theme="Natural-Wonders";
}
?>
<body id="home" class="<?=$theme;?>">
	<?=$boxlanguage;?>

	<?=$vheader2;?>

	<div class="container quiz_con">
		<div class="title">New Password</div>
		<form action="<?=base_url();?>password/update_password"  method="post" class="form">
	        <?php 
				if(isset($msg)){
					echo'<div class="notif">';
					echo $msg;
					echo'</div>';
				}
			?>
	        <strong>New Password</strong>
	        <input type="hidden" name="token" class="input" value="<?=$token;?>">
	        <input type="password" name="passwd" class="input"  placeholder="Your Password" required>
	        <div class="clearfix"></div>
	        <input type="submit" value="Update" class="btn_start">
	    </form>
		<div class="cleafix pt20"></div>
		<b><a href="<?=base_url();?>register" class="l_blue">Register</a></b> | 
     	<b><a href="<?=base_url();?>login" class="l_blue">Login</a></b>
	</div>

	<?=$alljs;?>
</body>
</html>
